package com.example.androidkotlin.db

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBHelper(context: Context, factory: SQLiteDatabase.CursorFactory?) :
    SQLiteOpenHelper(context, DATABASE_NAME, factory, DATABASE_VERSION) {

    override fun onCreate(db: SQLiteDatabase) {

        val query = ("CREATE TABLE " + TABLE_NAME + " ("
                + ID_COL + " TEXT PRIMARY KEY, " +
                NAME_COL + " TEXT," +
                DESCRIPTION_COL + " TEXT," +
                ADDRESS_COL + " TEXT," +
                TYPE_COL + " TEXT," +
                LATITUDE_COL + " TEXT," +
                LONGITUDE_COL + " TEXT," +
                SCORE_COL + " TEXT," +
                DISTANCE_COL + " TEXT" + ")")

        val queryMatch = ("CREATE TABLE " + "TABLE_MATCH" + " ("
                + "ID_USU" + " TEXT PRIMARY KEY, " +
                "RESP" + " TEXT" + ")")

        db.execSQL(query)
        db.execSQL(queryMatch)
    }

    override fun onUpgrade(db: SQLiteDatabase, p1: Int, p2: Int) {
        db.execSQL("DROP TABLE IF EXISTS $TABLE_NAME")
        db.execSQL("DROP TABLE IF EXISTS TABLE_MATCH")
        onCreate(db)
    }

    fun resetDB(){
        this.writableDatabase.delete(TABLE_NAME, null, null)
        this.writableDatabase.delete("TABLE_MATCH", null, null)
    }

    fun addPlace(documentId : String, name : String, description: String, address: String, type: String, latitude: String, longitude: String, score: String, distance: String){

        val values = ContentValues()

        values.put(ID_COL, documentId)
        values.put(NAME_COL, name)
        values.put(DESCRIPTION_COL, description)
        values.put(ADDRESS_COL, address)
        values.put(TYPE_COL, type)
        values.put(LATITUDE_COL, latitude)
        values.put(LONGITUDE_COL, longitude)
        values.put(SCORE_COL, score)
        values.put(DISTANCE_COL, distance)

        val db = this.writableDatabase

        db.insert(TABLE_NAME, null, values)

        db.close()
    }

    fun getPlaces(): Cursor? {

        val db = this.readableDatabase

        return db.rawQuery("SELECT * FROM $TABLE_NAME", null)

    }

    fun addMatch(userId : String, resp : String){
        val values = ContentValues()
        values.put("ID_USU", userId)
        values.put("RESP", resp)
        val db = this.writableDatabase
        db.insert("TABLE_MATCH", null, values)
        db.close()
    }

    fun getMatch(): Cursor? {
        val db = this.readableDatabase
        return db.rawQuery("SELECT * FROM TABLE_MATCH", null)
    }

    companion object{
        private const val DATABASE_NAME = "MY_PET_CARE"

        private const val DATABASE_VERSION = 1

        const val TABLE_NAME: String = "places_table"

        const val ID_COL: String = "documentId"

        const val NAME_COL: String = "name"

        const val DESCRIPTION_COL: String = "description"

        const val ADDRESS_COL: String = "address"

        const val TYPE_COL: String = "type"

        const val LATITUDE_COL: String = "latitude"

        const val LONGITUDE_COL: String = "longitude"

        const val SCORE_COL: String = "score"

        const val DISTANCE_COL: String = "distance"
    }
}
