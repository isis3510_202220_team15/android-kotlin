package com.example.androidkotlin.services

import android.content.Context
import android.util.Log
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.json.JSONArray
import org.json.JSONObject

object WeatherService {
    val WEATHER_API_BASE = "https://weatherapi-com.p.rapidapi.com/forecast.json"
    val WEATHER_API_KEY: String = "c9859d0bf8msh548168e44cd2cecp18d9efjsnf3f7e5696fcb"
    val WEATHER_CODES = arrayOf<Int>(1000, 1003, 1006)

    class HourlyWeather(
        val time: String,
        val temp: String,
        val condition: String,
        val icon: String
    ) {}

    suspend fun getForecast(context: Context, lat: String, lon: String, callbackSuccess: (String, String, MutableList<HourlyWeather>) -> Unit, callbackError: (String) -> Unit) {
        withContext(Dispatchers.IO) {
            val queue: RequestQueue = Volley.newRequestQueue(context)
            val url = "$WEATHER_API_BASE?key=$WEATHER_API_KEY&q=$lat,$lon"
            val request = object: JsonObjectRequest(Request.Method.GET, url, null, { response ->
                var lastUpdated = response.getJSONObject("current").getString("last_updated")
                var currentTemp = response.getJSONObject("current").getString("temp_c")

                var hourlyWeatherRaw: JSONArray = response.getJSONObject("forecast").getJSONArray("forecastday").getJSONObject(0).getJSONArray("hour")
                var hourlyWeather: MutableList<HourlyWeather> = processHourlyWeather(hourlyWeatherRaw)

                callbackSuccess(currentTemp, lastUpdated, hourlyWeather)
            }, { error ->
                callbackError(error.message!!)
            }) {
                override fun getHeaders(): MutableMap<String, String> {
                    val headers = HashMap<String, String>()
                    headers["X-RapidAPI-Key"] = "c9859d0bf8msh548168e44cd2cecp18d9efjsnf3f7e5696fcb"
                    headers["X-RapidAPI-Host"] = "weatherapi-com.p.rapidapi.com"
                    return headers
                }
            }
            queue.add(request)
        }
    }

    private fun processHourlyWeather(hourList: JSONArray): MutableList<HourlyWeather> {
        var hourlyWeather = mutableListOf<HourlyWeather>()
        for (i in 6 until 20) {
            var code = hourList.getJSONObject(i).getJSONObject("condition").getInt("code")
            if (code in WEATHER_CODES) {
                hourlyWeather.add(HourlyWeather(
                    hourList.getJSONObject(i).getString("time").split(" ")[1],
                    hourList.getJSONObject(i).getString("temp_c") + "°C",
                    hourList.getJSONObject(i).getJSONObject("condition").getString("text"),
                    hourList.getJSONObject(i).getJSONObject("condition").getString("icon").subSequence(39, 42).toString()
                ))
            }
        }
        return hourlyWeather
    }
}