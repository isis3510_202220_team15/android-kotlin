package com.example.androidkotlin.services

import com.example.androidkotlin.model.OCRInput
import com.example.androidkotlin.model.OCRResponse
import retrofit2.Call
import retrofit2.http.*

interface APIService {
    @POST("/")
    @Headers("Accept:application/json", "Content-Type:application/json")
    fun getOCRFromImage(@Body model: OCRInput): Call<OCRResponse>
}