package com.example.androidkotlin.services

import android.util.Log
import android.widget.Toast
import com.example.androidkotlin.model.*
import com.example.androidkotlin.network.TAG
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.functions.FirebaseFunctions
import com.google.firebase.functions.HttpsCallableResult
import com.google.firebase.functions.ktx.functions
import com.google.firebase.ktx.Firebase
import io.sentry.Sentry
import kotlinx.coroutines.tasks.await
import java.util.concurrent.CancellationException

object FirebaseService {

    fun getCurrentUserEmail(): String {
        val auth = FirebaseAuth.getInstance()
        return try {
            auth.currentUser!!.email.toString()
        } catch (e: Exception) {
            Sentry.captureException(e)
            throw CustomException(e.message.toString())
        }
    }

    suspend fun getCurrentUserData(): UserModel? {
        val db = FirebaseFirestore.getInstance()
        try {
            val transaction = Sentry.startTransaction("Fetch user data", "getCurrentUserData", true)
            val userObject = db.collection("users").document(getCurrentUserEmail()).get().await()
                .toObject<UserModel>()
            if (userObject != null) {
                userObject.email = getCurrentUserEmail()
            }
            transaction.finish()
            return userObject
        } catch (e: Exception) {
            Sentry.captureException(e)
            throw CancellationException()
        }
    }

    suspend fun getCurrentPetData(): PetModel {
        val db = FirebaseFirestore.getInstance()
        try {
            var petData =
                db.collection("users").document(getCurrentUserEmail()).collection("mascotas")
                    .document("Mascota1").get().await()
            if(petData.data == null){
                updatePetProfile("", "", "", "", "", ArrayList(), ArrayList())
                petData =
                    db.collection("users").document(getCurrentUserEmail()).collection("mascotas")
                        .document("Mascota1").get().await()
            }
            val petEvents : MutableList<EventModel> = ArrayList()
            for (eve in petData.data?.getValue("Events") as ArrayList<*>) {
                val eventName = (eve as HashMap<*, *>)["eventName"]
                val eventDate = (eve as HashMap<*, *>)["eventDate"]
                val eventDesc = (eve as HashMap<*, *>)["eventDescription"]
                val eventMod = EventModel(eventName as String, eventDate as String, eventDesc as String)
                petEvents.add(eventMod)
            }
            val petMatches : MutableList<String> = ArrayList()
            for (match in petData.data?.getValue("PetsLiked") as ArrayList<String>) {
                petMatches.add(match)
            }
            return PetModel(
                getCurrentUserEmail(),
                petData.data?.getValue("Image") as String,
                petData.data?.getValue("age") as String,
                petData.data?.getValue("breed") as String,
                petData.data?.getValue("name") as String,
                petData.data?.getValue("weight") as String,
                petEvents,
                petMatches
            )
        } catch (e: Exception) {
            throw CancellationException()
        }
    }

    suspend fun getCurrentPets(): MutableList<PetModel> {
        val db = FirebaseFirestore.getInstance()
        val listMasc: MutableList<PetModel> = ArrayList()
        var emailid = getCurrentUserEmail()
        try {
            var users = db.collection("users").get().await()
            for (pet in users){
                var PetData = db.collection("users").document(pet.id).collection("mascotas")
                    .document("Mascota1").get().await()
                if(PetData.data != null && pet.id != emailid){
                    var petDoc = PetModel(pet.id, "", PetData.data?.getValue("age").toString(), PetData.data?.getValue("breed").toString(), PetData.data?.getValue("name").toString(), PetData.data?.getValue("weight").toString(), ArrayList(), PetData.data?.getValue("PetsLiked") as ArrayList<String>)
                    listMasc.add(petDoc)
                }
            }
            return listMasc
        } catch (e: Exception) {
            throw CancellationException()
        }
    }

    suspend fun getPetsLiked(): MutableList<String>{
        val db = FirebaseFirestore.getInstance()
        try {
            var petData = db.collection("users").document(getCurrentUserEmail()).collection("mascotas")
                .document("Mascota1").get().await()
            return petData.data?.getValue("PetsLiked") as MutableList<String>
        } catch (e: Exception) {
            throw CancellationException()
        }
    }

    suspend fun getCurrentWalkersData(): List<WalkerModel> {
        val db = FirebaseFirestore.getInstance()
        val walkers: MutableList<WalkerModel> = ArrayList()
        try {

            val walkersObject = db.collection("walkers")
                .get().await()

            for (document in walkersObject) {
                walkers.add(document.toObject())
            }

            return walkers.toList()

        } catch (e: Exception) {
            throw CancellationException()
        }
    }

    suspend fun getWalkersDataByStringField(field: String, value: String): List<WalkerModel> {
        val db = FirebaseFirestore.getInstance()
        val walkers: MutableList<WalkerModel> = ArrayList()
        try {

            val walkersObject = db.collection("walkers")
                .whereEqualTo(field, value)
                .get().await()

            for (document in walkersObject) {
                walkers.add(document.toObject())
            }

            return walkers.toList()

        }  catch (e: Exception) {
            throw CancellationException()
        }
    }

    suspend fun getWalkersDataSorted(sortedBy: String, ascending: Boolean): List<WalkerModel>? {
        val db = FirebaseFirestore.getInstance()
        val walkers: MutableList<WalkerModel> = ArrayList()
        try {
            val direction = if (ascending) Query.Direction.ASCENDING;
                            else Query.Direction.DESCENDING

            val walkersObject = db.collection("walkers")
                .orderBy(sortedBy, direction)
                .limit(2)
                .get().await()

            for (document in walkersObject) {
                walkers.add(document.toObject())
            }

            return walkers.toList()

        } catch (e: Exception) {
            throw CustomException(e.message.toString())
        }
    }

    suspend fun incrementWalkerLikes(documentId: String) {
        val db = FirebaseFirestore.getInstance()

        val walkerObject = db.collection("walkers").document(documentId).get().await()
            .toObject<WalkerModel>()

        if (walkerObject != null) {
            if(!walkerObject.likes.contains(getCurrentUserEmail())){
                walkerObject.likes.add(getCurrentUserEmail())
                db.collection("walkers").document(documentId).update("likes", walkerObject.likes)
            }
        }
    }

    suspend fun decrementWalkerLikes(documentId: String) {
        val db = FirebaseFirestore.getInstance()

        val walkerObject = db.collection("walkers").document(documentId).get().await()
            .toObject<WalkerModel>()

        if (walkerObject != null) {
            if(walkerObject.likes.contains(getCurrentUserEmail())){
                walkerObject.likes.remove(getCurrentUserEmail())
                db.collection("walkers").document(documentId).update("likes", walkerObject.likes)
            }
        }
    }

    fun updateUserProfile(name: String, birthDate: String, phoneNumber: String) {
        val db = FirebaseFirestore.getInstance()
        val transaction = Sentry.startTransaction("Update user data", "updateUserProfile")
        db.collection("users").document(getCurrentUserEmail()).set(
            hashMapOf(
                "name" to if (name.trim().isNotEmpty()) name else "Name",
                "birthDate" to if (birthDate.trim().isNotEmpty()) birthDate else "Birth date",
                "phoneNumber" to if (phoneNumber.trim().isNotEmpty()) phoneNumber else "Telephone"
            )
        )
        transaction.finish()
    }

    fun updatePetProfile(Image: String, age: String, breed: String, name: String, weight: String, events: MutableList<EventModel>, petsLikes: MutableList<String>) {
        val db = FirebaseFirestore.getInstance()
        db.collection("users").document(getCurrentUserEmail()).collection("mascotas")
            .document("Mascota1").set(
                hashMapOf(
                    "Image" to if (Image.trim().isNotEmpty()) Image else "",
                    "age" to if (age.trim().isNotEmpty()) age else "",
                    "breed" to if (breed.trim().isNotEmpty()) breed else "",
                    "name" to if (name.trim().isNotEmpty()) name else "",
                    "weight" to if (weight.trim().isNotEmpty()) weight else "",
                    "Events" to if (events.isNotEmpty()) events else ArrayList(),
                    "PetsLiked" to if (petsLikes.isNotEmpty()) petsLikes else ArrayList()
                )
            )
    }

    fun updatePetImage(Image: String) {
        val db = FirebaseFirestore.getInstance()
        db.collection("users").document(getCurrentUserEmail()).collection("mascotas")
            .document("Mascota1").update("Image", Image)
    }

    fun updateEvent(Events: MutableList<EventModel>){
        val db = FirebaseFirestore.getInstance()
        db.collection("users").document(getCurrentUserEmail()).collection("mascotas")
            .document("Mascota1").update("Events", Events)
    }

    fun updatePetsLiked(PetsLiked: MutableList<String>){
        val db = FirebaseFirestore.getInstance()
        db.collection("users").document(getCurrentUserEmail()).collection("mascotas")
            .document("Mascota1").update("PetsLiked", PetsLiked)
    }

    fun signIn(email: String, password: String) {
        val auth = FirebaseAuth.getInstance()
        auth.signInWithEmailAndPassword(email, password)
    }

    fun updateWalkerProfile(email: String, name: String, city: String, neighborhood: String, description: String, address: String, lat: Float, lon: Float, radius: Int) {
        val db = FirebaseFirestore.getInstance()
        db.collection("walkers").document(email).set(
            hashMapOf(
                "city" to if (city.trim().isNotEmpty()) name else "City",
                "description" to if (description.trim().isNotEmpty()) description else "Description",
                "name" to if (name.trim().isNotEmpty()) name else "name",
                "neighborhood" to if (neighborhood.trim().isNotEmpty()) neighborhood else "Neighborhood",
                "likes" to FieldValue.arrayUnion(),
                "address" to if (address.trim().isNotEmpty()) address else "address",
                "latitude" to lat,
                "longitude" to lon,
                "radius" to radius
            )
        )
    }

    suspend fun getWalkerProfile(): WalkerModel? {
        try {
            val db = FirebaseFirestore.getInstance()
            return db.collection("walkers").document(getCurrentUserEmail()).get().await()
                .toObject<WalkerModel>()
        } catch (e: Exception) {
        throw CancellationException()
        }
    }
}

class CustomException(message: String) : Exception(message)