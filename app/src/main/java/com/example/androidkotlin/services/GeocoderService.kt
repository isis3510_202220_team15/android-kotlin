package com.example.androidkotlin.services

import android.content.Context
import android.location.Address
import android.location.Geocoder
import com.google.android.gms.maps.model.LatLng
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object GeocoderService {
    private lateinit var geocoder: Geocoder

    suspend fun getAddress(
        context: Context,
        latlong: LatLng,
        onSuccess: (String) -> Unit,
        onError: () -> Unit
    ) {
        geocoder = Geocoder(context)
        withContext(Dispatchers.IO) {
            val resp: MutableList<Address>? =
                geocoder.getFromLocation(latlong.latitude, latlong.longitude, 1)
            if (resp != null) {
                val address: String = resp[0].getAddressLine(0)
                onSuccess(address)
            } else {
                onError()
            }
        }
    }
}