package com.example.androidkotlin.view

import android.content.Context
import android.location.Location
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.androidkotlin.adapters.PlacesRecyclerViewAdapter
import com.example.androidkotlin.databinding.FragmentPlaceItemListBinding
import com.example.androidkotlin.db.DBHelper
import com.example.androidkotlin.model.PlaceModel
import com.example.androidkotlin.viewmodel.PlacesViewModel
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices

class PlacesListFragment() : Fragment() {

    private var columnCount = 1
    private val placesViewModel: PlacesViewModel by activityViewModels()
    private val adapter: PlacesRecyclerViewAdapter? = null
    private lateinit var binding: FragmentPlaceItemListBinding
    private lateinit var parentContext: MainActivity
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var db: DBHelper

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is MainActivity) {
            parentContext = context
        } else {
            throw RuntimeException("$context attach error")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(parentContext)

        val parent = requireActivity()
        db = DBHelper(parent, null)
        arguments?.let {
            columnCount = it.getInt(ARG_COLUMN_COUNT)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentPlaceItemListBinding.inflate(inflater, container, false)
        val view = binding.placesRecyclerview
        // Set the adapter
        with(view) {
            layoutManager = when {
                columnCount <= 1 -> LinearLayoutManager(context)
                else -> GridLayoutManager(context, columnCount)
            }

            placesViewModel.places.observe(viewLifecycleOwner) { currentPlaces ->
                adapter = PlacesRecyclerViewAdapter(currentPlaces)
                val noConnectionMessage = binding.noConnection.noConnection
                if (currentPlaces.isEmpty()) {
                    noConnectionMessage.visibility = View.VISIBLE
                }
            }
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val parent = requireActivity()

        if (parent is MainActivity) {
            val noConnectionMessage = binding.noConnection.noConnection
            val outdatedData = binding.outdatedData.outdatedData
            val currPlaces: List<PlaceModel>? = placesViewModel.places.value
            parent.connectionLiveData.observe(parent) { isNetworkAvailable ->
                isNetworkAvailable.let { isAvailable ->
                    if (!isAvailable && currPlaces == null) {
                        val itemNames = mutableListOf<PlaceModel>()
                        val cursor = db.getPlaces()
                        with(cursor) {
                            while (this?.moveToNext() == true) {
                                val place =  PlaceModel(
                                    documentId = getString(getColumnIndexOrThrow(DBHelper.ID_COL)),
                                    name = getString(getColumnIndexOrThrow(DBHelper.NAME_COL)),
                                    description = getString(getColumnIndexOrThrow(DBHelper.DESCRIPTION_COL)),
                                    address = getString(getColumnIndexOrThrow(DBHelper.ADDRESS_COL)),
                                    type = getString(getColumnIndexOrThrow(DBHelper.TYPE_COL)),
                                    latitude = getString(getColumnIndexOrThrow(DBHelper.LATITUDE_COL)),
                                    longitude = getString(getColumnIndexOrThrow(DBHelper.LONGITUDE_COL)),
                                    score = getString(getColumnIndexOrThrow(DBHelper.SCORE_COL)),
                                    distance = getString(getColumnIndexOrThrow(DBHelper.DISTANCE_COL)),
                                )
                                itemNames.add(place)
                            }
                        }
                        cursor?.close()
                        if(itemNames.isNotEmpty()){
                            placesViewModel.setPlacesData(itemNames.toList())
                            noConnectionMessage.visibility = View.GONE
                            outdatedData.visibility = View.VISIBLE
                        } else {
                            noConnectionMessage.visibility = View.VISIBLE
                            outdatedData.visibility = View.GONE
                        }
                    } else {
                        try {
                            fusedLocationClient.lastLocation
                                .addOnSuccessListener { location: Location? ->
                                    if (location != null) {
                                        placesViewModel.setLatitude(location.latitude)
                                        placesViewModel.setLongitude(location.longitude)
                                        placesViewModel.updatePlacesData(location.latitude, location.longitude)
                                        outdatedData.visibility = View.GONE
                                    }
                                }
                        } catch (e: SecurityException) {
                            e.printStackTrace()
                        }
                        noConnectionMessage.visibility = View.GONE
                    }
                }
            }
        }
    }

    companion object {


        const val ARG_COLUMN_COUNT = "column-count"


        @JvmStatic
        fun newInstance(columnCount: Int) =
            PlacesListFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_COLUMN_COUNT, columnCount)
                }
            }
    }
}