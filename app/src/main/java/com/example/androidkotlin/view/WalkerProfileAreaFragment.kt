package com.example.androidkotlin.view

import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Geocoder
import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.androidkotlin.R
import com.example.androidkotlin.databinding.FragmentWalkerProfileAreaBinding
import com.example.androidkotlin.services.GeocoderService
import com.example.androidkotlin.viewmodel.WalkersViewModel
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch

class WalkerProfileAreaFragment : Fragment(), OnMapReadyCallback {
    private var _binding: FragmentWalkerProfileAreaBinding? = null
    private lateinit var map: GoogleMap
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private val walkersViewModel : WalkersViewModel by viewModels()

    private lateinit var parentContext: MainActivity

    private val latlng = MutableStateFlow<LatLng?>(null)
    private var marker : Marker? = null
    private val radius = MutableStateFlow<Int>(50)
    private var circle : Circle? = null
    private var address : String = ""

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is MainActivity) {
            parentContext = context
        } else {
            throw RuntimeException("$context attach error")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(parentContext)
    }

    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentWalkerProfileAreaBinding.inflate(inflater, container, false)

        val mapFragment : SupportMapFragment = childFragmentManager.findFragmentById(R.id.map_fragment) as SupportMapFragment //binding.mapFragment.getFragment()
        mapFragment.getMapAsync(this)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.setButton.setOnClickListener {
            walkersViewModel.saveWalkerProfileInCache(null, null, null, null, null, address, latlng.value!!.latitude.toFloat(), latlng.value!!.longitude.toFloat(), radius.value)
            //parentFragmentManager.popBackStackImmediate()
            findNavController().navigate(R.id.action_walkerProfileAreaFragment2_to_walkerProfileFragment)
        }

        binding.radiusSeekbar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                val r = (progress * 50) + 50
                binding.placeRadiusText.text = "Walker radius: " + r + " m"
                circle?.radius = r.toDouble()
                radius.value = r
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {}

            override fun onStopTrackingTouch(seekBar: SeekBar?) {}

        })
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap

        map.setOnMapLongClickListener {
            latlng.value = it
        }

        lifecycleScope.launch {
            latlng.collect { value ->
                if (value != null) {
                    marker?.remove()
                    map.animateCamera(CameraUpdateFactory.newLatLngZoom(value, 15f))
                    marker = map.addMarker(MarkerOptions().position(value))
                    circle?.remove()
                    circle = map.addCircle(CircleOptions()
                        .center(value)
                        .radius(radius.value.toDouble())
                        .strokeWidth(8f))

                    lifecycleScope.launch {
                        GeocoderService.getAddress(
                            requireContext(),
                            value,
                            {ans -> requireActivity().runOnUiThread {
                                binding.placeAddressText.text = "Address: $ans"
                                address = ans
                            }},
                            {requireActivity().runOnUiThread {
                                binding.placeAddressText.text = "Failed to retrieve address."
                            }}
                        )
                    }
                }
            }
        }

        checkLocationPermission()
    }

    private fun checkLocationPermission() {
        val permission = Manifest.permission.ACCESS_FINE_LOCATION
        val granted = ActivityCompat.checkSelfPermission(requireContext(), permission)
        if (granted != PackageManager.PERMISSION_GRANTED) {

            onLocationUnavailable()

            if (ActivityCompat.shouldShowRequestPermissionRationale(requireActivity(), permission)) {
                AlertDialog.Builder(requireContext())
                    .setTitle("Location permission required")
                    .setMessage("This app needs access to your location to enable the weather suggestion service")
                    .setPositiveButton("OK") { _, _ -> requestPermissionLauncher.launch(permission)}
                    .setNegativeButton("Cancel", null)
                    .create()
                    .show()
            }
            requestPermissionLauncher.launch(permission)

        } else {
            onLocationAccessGranted()
        }
    }

    private val requestPermissionLauncher =
        registerForActivityResult(
            ActivityResultContracts.RequestPermission()
        ) { isGranted: Boolean ->
            if (isGranted) {
                onLocationAccessGranted()
            } else {
                onLocationUnavailable()
            }
        }

    private fun onLocationAccessGranted() {
        binding.noLocation.noLocation.visibility = View.GONE

        binding.titleText.visibility = View.VISIBLE
        binding.mapFragment.visibility = View.VISIBLE
        binding.radiusSeekbar.visibility = View.VISIBLE
        binding.setButton.visibility = View.VISIBLE

        try {
            fusedLocationClient.lastLocation.addOnSuccessListener { location ->
                if (location != null) {
                    latlng.value = LatLng(location.latitude, location.longitude)
                }
            }
        } catch (e: SecurityException) {
            e.printStackTrace()
        }
    }

    private fun onLocationUnavailable() {
        binding.titleText.visibility = View.GONE
        binding.mapFragment.visibility = View.GONE
        binding.radiusSeekbar.visibility = View.GONE
        binding.setButton.visibility = View.GONE

        binding.noLocation.noLocation.visibility = View.VISIBLE
    }
}