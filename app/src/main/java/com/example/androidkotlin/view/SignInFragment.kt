package com.example.androidkotlin.view

import android.content.Context
import android.os.Bundle
import android.transition.TransitionInflater
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKeys
import com.example.androidkotlin.R
import com.example.androidkotlin.databinding.FragmentSignInBinding
import com.example.androidkotlin.viewmodel.AuthViewModel
import com.google.firebase.auth.FirebaseAuth

import io.sentry.Sentry


class SignInFragment : Fragment() {
    private lateinit var  auth: FirebaseAuth
    private lateinit var binding: FragmentSignInBinding
    private val authViewModel : AuthViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        val inflater = TransitionInflater.from(requireContext())
        enterTransition = inflater.inflateTransition(R.transition.slide_in_right)
        super.onCreate(savedInstanceState)
        Sentry.captureMessage("testing SDK setup")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentSignInBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE) ?: return
        val savedEmail = sharedPref.getString(getString(R.string.saved_email_key), "")
        binding.editTextEmailAddress.setText(savedEmail)

        val keyGenParameterSpec = MasterKeys.AES256_GCM_SPEC
        val mainKeyAlias = MasterKeys.getOrCreate(keyGenParameterSpec)

        val securePreferences = EncryptedSharedPreferences.create(
            "secure_prefs",
            mainKeyAlias,
            requireActivity().applicationContext,
            EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
            EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
        )
        val savedPassword = securePreferences.getString(getString(R.string.saved_pasword_key), "")
        binding.editTextPassword.setText(savedPassword)

        authViewModel.authState.observe(viewLifecycleOwner, Observer { authState ->

                if(authState == AuthViewModel.AuthState.SuccessLogin){
                    findNavController().navigate(R.id.action_signInFragment_to_FirstFragment)
                    with (sharedPref.edit()) {
                        putString(getString(R.string.saved_email_key), binding.editTextEmailAddress.text.toString().trim())
                        apply()
                    }
                    with(securePreferences.edit()) {
                        putString(getString(R.string.saved_pasword_key), binding.editTextPassword.text.toString().trim() )
                        apply()
                    }
                    binding.progressbar.visibility = View.GONE
                }
                if(authState is AuthViewModel.AuthState.AuthError){
                    binding.progressbar.visibility = View.GONE
                    Toast.makeText(activity,authState.exceptionMessage, Toast.LENGTH_LONG).show()
                }
        })

        binding.buttonLogin.setOnClickListener {

            val parent = requireActivity()
            if (parent is MainActivity) {
                if (parent.connectionLiveData.value == false) {
                    Toast.makeText(
                        activity,
                        "You don't have internet connection. Check your internet and try again",
                        Toast.LENGTH_LONG
                    ).show()
                } else {
                    binding.progressbar.visibility = View.VISIBLE

                    if(binding.editTextEmailAddress.text.toString().trim().isNotEmpty() && binding.editTextPassword.text.toString().trim().isNotEmpty()){
                        authViewModel.signIn(
                            binding.editTextEmailAddress.text.toString().trim(),
                            binding.editTextPassword.text.toString().trim())
                    }
                    else{
                        binding.progressbar.visibility = View.GONE
                        Toast.makeText(activity,"Fields cannot be empty", Toast.LENGTH_LONG).show()
                    }
                }
            }
        }

        binding.textViewRegister.setOnClickListener {
            findNavController().navigate(R.id.action_signInFragment_to_signUpFragment)
        }
    }

}