package com.example.androidkotlin.view

import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Base64
import android.util.LruCache
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.androidkotlin.databinding.FragmentPetDocumentBinding
import com.example.androidkotlin.model.OCRInput
import com.example.androidkotlin.model.OCRResponse
import com.example.androidkotlin.viewmodel.PetViewModel
import com.google.android.gms.tasks.Task
import com.google.common.hash.HashCode
import com.google.firebase.functions.FirebaseFunctions
import com.google.firebase.functions.ktx.functions
import com.google.firebase.ktx.Firebase
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.JsonParser
import java.security.MessageDigest

class PetDocumentFragment : Fragment() {
    private val petViewModel : PetViewModel by viewModels()
    private lateinit var binding: FragmentPetDocumentBinding
    private lateinit var functions: FirebaseFunctions
    val lru: LruCache<Any, Any> = LruCache(DEFAULT_BUFFER_SIZE)
    var base64image: String = ""

    private object HOLDER {
        val INSTANCE = PetDocumentFragment()
    }

    companion object {
        val instance: PetDocumentFragment by lazy { HOLDER.INSTANCE }
    }

    private fun annotateImage(requestJson: String): Task<JsonElement> {
        return functions
            .getHttpsCallable("annotateImage")
            .call(requestJson)
            .continueWith { task ->
                // This continuation runs on either success or failure, but if the task
                // has failed then result will throw an Exception which will be
                // propagated down.
                val result = task.result?.data
                JsonParser.parseString(Gson().toJson(result))
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViewModel()
        functions = Firebase.functions
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentPetDocumentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        petViewModel.petProfile.observe(viewLifecycleOwner, Observer { currentPet ->
            val imageBytes = Base64.decode(currentPet.Image, Base64.DEFAULT)
            val decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
            base64image = MessageDigest.getInstance("SHA256").digest(currentPet.Image?.toByteArray()).toString()
            if(instance.lru.get(base64image) != null){
                binding.petDocumentOcr.text = instance.lru.get(base64image).toString()
            }
            binding.imageView.setImageBitmap(decodedImage)
        })
        if(instance.lru.get(base64image) != null){
            binding.petDocumentOcr.text = instance.lru.get(base64image).toString()
        }
        binding.buttonDetectText.setOnClickListener {
            if(instance.lru.get(base64image) != null){
                binding.petDocumentOcr.text = instance.lru.get(base64image).toString()
            }
            else{
                petViewModel.petProfile.observe(viewLifecycleOwner, Observer { currentPet ->
                    petViewModel.ocrPetImage(OCRInput(currentPet.Image.toString()))
                })
            }
        }
    }

    private fun initViewModel(){
        val viewModel = ViewModelProvider(this)[PetViewModel::class.java]
        viewModel.getOcrPetLiveDataObserver().observe(this, Observer<OCRResponse?>{
            if(it == null){
                binding.petDocumentOcr.text = "OCR could not be calculated because there is no internet"
            }
            else{
                instance.lru.put(base64image,it.texto)
                binding.petDocumentOcr.text = it.texto
            }
        })
    }
}