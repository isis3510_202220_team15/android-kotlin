package com.example.androidkotlin.view

import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.androidkotlin.R
import com.example.androidkotlin.adapters.WeatherCardAdapter
import com.example.androidkotlin.databinding.FragmentFirstBinding
import com.example.androidkotlin.model.EventModel
import com.example.androidkotlin.services.FirebaseService
import com.example.androidkotlin.services.WeatherService
import com.example.androidkotlin.viewmodel.PetViewModel
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import io.sentry.Sentry
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.ZoneId
import java.util.*

class FirstFragment : Fragment() {
    private var _binding: FragmentFirstBinding? = null
    private lateinit var parentContext: MainActivity
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private val petViewModel : PetViewModel by viewModels()
    private var petEvents : MutableList<String> = ArrayList()

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is MainActivity) {
            parentContext = context
        } else {
            throw RuntimeException("$context attach error")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(parentContext)
        try {
            throw Exception("This is a test.")
        } catch (e: Exception) {
            Sentry.captureException(e)
        }
    }

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentFirstBinding.inflate(inflater, container, false)

        return binding.root
    }

    @OptIn(DelicateCoroutinesApi::class)
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.cardViewContainer.cardView.setOnClickListener{
            findNavController().navigate(R.id.action_FirstFragment_to_petProfileFragment)
        }

        binding.eventButton.setOnClickListener {
            findNavController().navigate(R.id.action_FirstFragment_to_eventFragment)
        }

        binding.suggestionsEnableLocationPermissionButton.setOnClickListener {
            checkLocationPermission()
        }

        binding.suggestionsRefreshButton.setOnClickListener {
            checkLocationPermission()
        }

        petViewModel.obtenerNuevoPet()
        checkLocationPermission()

        petViewModel.petProfile.observe(viewLifecycleOwner, Observer { currentPet ->
            if(currentPet != null && currentPet.Events.isNotEmpty()){
                petEvents = arrayListOf()
                for (ev in currentPet.Events){
                    if(SimpleDateFormat( "dd-MM-yyyy").parse(ev.eventDate) >= Date.from(LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant()) && SimpleDateFormat( "dd-MM-yyyy").parse(ev.eventDate) <= Date.from(LocalDate.now().plusDays(3).atStartOfDay(ZoneId.systemDefault()).toInstant())){
                        petEvents.add(ev.eventName + " el " + ev.eventDate)
                    }
                }
                val arrayAdapter: ArrayAdapter<*>
                arrayAdapter = ArrayAdapter(parentContext, android.R.layout.simple_list_item_1, petEvents)
                binding.eventList.adapter = arrayAdapter
            }
            if(currentPet.name.toString().isNullOrEmpty()){
                binding.eventButton.visibility = View.INVISIBLE
            }
            else{
                binding.eventButton.visibility = View.VISIBLE
            }
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun checkLocationPermission() {
        val permission = Manifest.permission.ACCESS_COARSE_LOCATION
        var granted = ActivityCompat.checkSelfPermission(requireContext(), permission);
        if (granted != PackageManager.PERMISSION_GRANTED) {

            onLocationAccessNotGranted()

            if (ActivityCompat.shouldShowRequestPermissionRationale(requireActivity(), permission)) {
                AlertDialog.Builder(requireContext())
                    .setTitle("Location permission required")
                    .setMessage("This app needs access to your location to enable the weather suggestion service")
                    .setPositiveButton("OK") { _, _ -> requestPermissionLauncher.launch(permission)}
                    .setNegativeButton("Cancel", null)
                    .create()
                    .show()
            }
            requestPermissionLauncher.launch(permission)

        } else {
            onLocationAccessGranted()
        }
    }

    private val requestPermissionLauncher =
        registerForActivityResult(
            ActivityResultContracts.RequestPermission()
        ) { isGranted: Boolean ->
            if (isGranted) {
                onLocationAccessGranted()
            } else {
                onLocationAccessNotGranted()
            }
        }

    private fun onLocationAccessGranted() {
        binding.suggestionsEnableLocationPermissionButton.visibility = View.GONE
        binding.suggestionsErrorText.visibility = View.GONE
        binding.suggestionsProgressbar.visibility = View.VISIBLE
        try {
            fusedLocationClient.lastLocation
                .addOnSuccessListener { location : Location? ->
                    if (location != null) {
                        lifecycleScope.launch {
                            WeatherService.getForecast(
                                requireContext(),
                                location.latitude.toString(),
                                location.longitude.toString(),
                                {currentTemp, lastUpdated, hourlyWeather ->
                                    binding.suggestionsDataText.text = "Current temperature: ${currentTemp}\n" +
                                            "Last updated: ${lastUpdated}\n" +
                                            "Take your pets for a walk at the following times:"
                                    binding.suggestionsDataText.visibility = View.VISIBLE
                                    if (hourlyWeather.isEmpty()) {
                                        binding.suggestionsProgressbar.visibility = View.GONE
                                        binding.suggestionsErrorText.text = "The weather isn't optimal for taking your pets for a walk today."
                                        binding.suggestionsErrorText.visibility = View.VISIBLE
                                    }
                                    else {
                                        binding.WeatherRecyclerView.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
                                        //binding.WeatherRecyclerView.layoutManager = GridLayoutManager(requireContext(), 2)
                                        binding.WeatherRecyclerView.adapter = WeatherCardAdapter(hourlyWeather)
                                        binding.suggestionsProgressbar.visibility = View.GONE
                                        binding.WeatherRecyclerView.visibility = View.VISIBLE
                                    }},
                                {
                                    binding.suggestionsProgressbar.visibility = View.GONE
                                    binding.suggestionsDataText.visibility = View.GONE
                                    binding.suggestionsErrorText.text = "Check your internet connection and try again later."
                                    binding.suggestionsErrorText.visibility = View.VISIBLE
                                }
                            )
                        }
                    }
                }
        }
        catch(e: SecurityException) {
            e.printStackTrace()
        }
    }

    private fun onLocationAccessNotGranted() {
        binding.WeatherRecyclerView.visibility = View.GONE
        binding.suggestionsDataText.visibility = View.GONE
        binding.suggestionsErrorText.text = "Enable location access to enable the weather service."
        binding.suggestionsErrorText.visibility = View.VISIBLE
    }
}