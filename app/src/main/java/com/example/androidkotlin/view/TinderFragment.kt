package com.example.androidkotlin.view

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.example.androidkotlin.R
import com.example.androidkotlin.databinding.FragmentTinderBinding
import com.example.androidkotlin.model.EventModel
import com.example.androidkotlin.model.PetModel
import com.example.androidkotlin.network.ConnectionLiveData
import com.example.androidkotlin.network.TAG
import com.example.androidkotlin.viewmodel.PetViewModel
import com.google.firebase.functions.FirebaseFunctions
import com.google.firebase.functions.ktx.functions
import com.google.firebase.ktx.Firebase
import java.util.ArrayList

class TinderFragment : Fragment() {
    private val petViewModel : PetViewModel by viewModels()
    private lateinit var binding: FragmentTinderBinding
    private lateinit var functions: FirebaseFunctions
    private lateinit var parentContext: MainActivity
    private var pets : MutableList<PetModel> = ArrayList()
    private var petsLiked: MutableList<String> = ArrayList()
    private var posicion : Int = 0

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is MainActivity) {
            parentContext = context
        } else {
            throw RuntimeException("$context attach error")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        functions = Firebase.functions
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentTinderBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.tinderProgressbar.visibility = View.VISIBLE
        binding.cardView.visibility = View.GONE
        binding.buttonMatch.visibility = View.GONE

        parentContext.connectionLiveData.observe(viewLifecycleOwner) { isNetworkAvailable ->
            isNetworkAvailable.let { isAvalilable ->
                if (!isAvalilable){
                    binding.tinderProgressbar.visibility = View.GONE
                    binding.cardView.visibility = View.GONE
                    binding.titleMessage.visibility = View.VISIBLE
                    binding.buttonMatch.visibility = View.VISIBLE
                    binding.titleMessage.text = "There is no internet connection"
                }
                else {
                    petViewModel.getPets()
                    petViewModel.getPetsLiked()
                    binding.tinderProgressbar.visibility = View.VISIBLE
                    binding.titleMessage.text = ""
                    binding.cardView.visibility = View.GONE
                }
            }
        }

        binding.buttonMatch.setOnClickListener {
            findNavController().navigate(R.id.action_tinderFragment_to_matchFragment)
        }

        petViewModel.petsLiked.observe(viewLifecycleOwner, Observer { currentPetLiked ->
            if(currentPetLiked != null){
                petsLiked = currentPetLiked
            }
        })

        petViewModel.petsData.observe(viewLifecycleOwner, Observer { currentPet ->
            if(currentPet.size > 0){
                for(pet in currentPet){
                    if(!petsLiked.contains(pet.userId)){
                        pets.add(pet)
                    }
                }
                if(pets.size > 0){
                    binding.NameText.text = pets[posicion].name
                    binding.AgeText.text = pets[posicion].age
                    binding.BreedText.text = pets[posicion].breed
                    binding.WeightText.text = pets[posicion].weight
                    binding.tinderProgressbar.visibility = View.GONE
                    binding.cardView.visibility = View.VISIBLE
                    binding.buttonMatch.visibility = View.VISIBLE
                }
                else{
                    binding.titleMessage.text = "There are no more pets in the app"
                    binding.cardView.visibility = View.GONE
                    binding.tinderProgressbar.visibility = View.GONE
                    binding.buttonMatch.visibility = View.VISIBLE
                }
            }
        })

        binding.buttonLike.setOnClickListener {
            petsLiked.add(pets[posicion].userId.toString())
            pets.removeAt(posicion)
            petViewModel.createPetsLiked(petsLiked)
            if(pets.size > 0){
                binding.NameText.text = pets[posicion].name
                binding.AgeText.text = pets[posicion].age
                binding.BreedText.text = pets[posicion].breed
                binding.WeightText.text = pets[posicion].weight
                binding.tinderProgressbar.visibility = View.GONE
            }
            else{
                binding.titleMessage.text = "There are no more pets in the app"
                binding.cardView.visibility = View.GONE
                binding.tinderProgressbar.visibility = View.GONE
                binding.buttonMatch.visibility = View.VISIBLE
            }
        }

        binding.buttonDislike.setOnClickListener {
            if(posicion < pets.size-1){
                posicion += 1
            }
            else{
                posicion = 0
            }
            binding.NameText.text = pets[posicion].name
            binding.AgeText.text = pets[posicion].age
            binding.BreedText.text = pets[posicion].breed
            binding.WeightText.text = pets[posicion].weight
        }
    }
}