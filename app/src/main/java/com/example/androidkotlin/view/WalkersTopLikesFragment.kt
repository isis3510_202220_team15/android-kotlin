package com.example.androidkotlin.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.androidkotlin.adapters.MyItemRecyclerViewAdapter
import com.example.androidkotlin.databinding.FragmentWalkersTopLikesBinding
import com.example.androidkotlin.viewmodel.WalkersViewModel

class WalkersTopLikesFragment : Fragment() {
    private var columnCount = 1
    private val walkersViewModel: WalkersViewModel by viewModels()
    private val adapter: MyItemRecyclerViewAdapter? = null
    private lateinit var binding: FragmentWalkersTopLikesBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            columnCount = it.getInt(WalkersListFragment.ARG_COLUMN_COUNT)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentWalkersTopLikesBinding.inflate(inflater, container, false)
        val view = binding.walkersTopLikesRecyclerview
        // Set the adapter
        with(view) {
            layoutManager = when {
                columnCount <= 1 -> LinearLayoutManager(context)
                else -> GridLayoutManager(context, columnCount)
            }

            walkersViewModel.updateCurrentWalkersDataSorted("likes", false, 5)

            walkersViewModel.walkers.observe(viewLifecycleOwner) { currentWalkers ->
                adapter = MyItemRecyclerViewAdapter(currentWalkers, walkersViewModel)
                val noConnectionMessage = binding.noConnection.noConnection
                noConnectionMessage.visibility = View.GONE
                if (currentWalkers.isEmpty()) {
                    noConnectionMessage.visibility = View.VISIBLE
                }
            }
        }

        return binding.root
    }




    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val parent = requireActivity()

        if (parent is MainActivity) {
            val noConnectionMessage = binding.noConnection.noConnection
            val outdatedData = binding.outdatedData.outdatedData
            parent.connectionLiveData.observe(parent) { isNetworkAvailable ->
                isNetworkAvailable.let { isAvailable ->
                    if (!isAvailable && walkersViewModel.walkers.value != null) {
                        outdatedData.visibility = View.VISIBLE
                        noConnectionMessage.visibility = View.GONE

                    } else if(isAvailable){
                        outdatedData.visibility = View.GONE
                        noConnectionMessage.visibility = View.GONE
                        walkersViewModel.updateCurrentWalkersData()
                    } else if(!isAvailable && walkersViewModel.walkers.value == null){
                        noConnectionMessage.visibility = View.VISIBLE
                    }
                }
            }
        }
    }
}