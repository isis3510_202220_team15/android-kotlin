package com.example.androidkotlin.view

import android.content.Context
import android.os.Bundle
import android.telephony.PhoneNumberUtils
import android.text.Editable
import android.text.TextWatcher
import android.transition.TransitionInflater
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKeys
import com.example.androidkotlin.R
import com.example.androidkotlin.databinding.FragmentSignUpBinding
import com.example.androidkotlin.viewmodel.AuthViewModel
import com.example.androidkotlin.viewmodel.UserViewModel
import com.google.android.material.datepicker.CalendarConstraints
import com.google.android.material.datepicker.DateValidatorPointBackward
import com.google.android.material.datepicker.MaterialDatePicker
import java.text.SimpleDateFormat
import java.util.*

class SignUpFragment : Fragment() {
    private lateinit var binding: FragmentSignUpBinding
    private val authViewModel: AuthViewModel by viewModels()
    private val userViewModel: UserViewModel by viewModels()

    private lateinit var parentContext: MainActivity

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is MainActivity) {
            parentContext = context
        } else {
            throw RuntimeException("$context attach error")
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        val inflater = TransitionInflater.from(requireContext())
        enterTransition = inflater.inflateTransition(R.transition.slide_in_right)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSignUpBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE) ?: return

        val keyGenParameterSpec = MasterKeys.AES256_GCM_SPEC
        val mainKeyAlias = MasterKeys.getOrCreate(keyGenParameterSpec)

        val securePreferences = EncryptedSharedPreferences.create(
            "secure_prefs",
            mainKeyAlias,
            requireActivity().applicationContext,
            EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
            EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
        )

        authViewModel.authState.observe(viewLifecycleOwner) { authState ->


            if (authState == AuthViewModel.AuthState.SuccessSignUp) {
                authViewModel.signIn(
                    binding.editTextEmailAddressSignUp.text.toString().trim(),
                    binding.editTextPasswordSignUp.text.toString().trim()
                )
            }
            if (authState == AuthViewModel.AuthState.SuccessLogin) {
                findNavController().navigate(R.id.action_signUpFragment_to_FirstFragment)
                userViewModel.updateUserProfile(
                    binding.nameTextField.editText?.text.toString(),
                    binding.birthDateTextView.text.toString(),
                    binding.phoneNumberTextField.editText?.text.toString()
                )
                binding.progressbar.visibility = View.GONE
                with(sharedPref.edit()) {
                    putString(
                        getString(R.string.saved_email_key),
                        binding.editTextEmailAddressSignUp.text.toString().trim()
                    )
                    apply()
                }

                with(securePreferences.edit()) {
                    putString(
                        getString(R.string.saved_pasword_key),
                        binding.editTextPasswordSignUp.text.toString().trim()
                    )
                    apply()
                }
            }
            if (authState is AuthViewModel.AuthState.AuthError) {
                Toast.makeText(activity, authState.exceptionMessage, Toast.LENGTH_LONG).show()
            }
        }

        binding.birthDateTextView.setOnClickListener {
            Locale.setDefault(Locale.ENGLISH)
            val constraintsBuilder = CalendarConstraints.Builder().setValidator(
                DateValidatorPointBackward.now()
            )
            val datePicker = MaterialDatePicker.Builder.datePicker()
                .setCalendarConstraints(constraintsBuilder.build())
                .setTitleText("Select date of birth").build()
            activity?.let { it1 -> datePicker.show(it1.supportFragmentManager, "DatePicker") }

            // Setting up the event for when ok is clicked
            datePicker.addOnPositiveButtonClickListener { date ->
                val dateFormatter = SimpleDateFormat("dd-MM-yyyy")
                val formattedDate = dateFormatter.format(Date(date))
                binding.birthDateTextView.text = formattedDate
            }
        }

        var isValidaded = false

        binding.textViewLogin.setOnClickListener {
            findNavController().navigate(R.id.action_signUpFragment_to_signInFragment)
        }

        binding.phoneNumberTextField.editText?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                isValidaded = false
                if (s != null) {
                    if (s.isEmpty()) {
                        binding.phoneNumberTextField.error = "     Telephone is required!"
                    } else if (s.length in 0..9) {
                        binding.phoneNumberTextField.error = "     Incorrect telephone format"
                    } else if (!PhoneNumberUtils.isGlobalPhoneNumber(s.toString())) {
                        binding.phoneNumberTextField.error = "     Incorrect telephone format"
                    } else {
                        binding.phoneNumberTextField.error = null
                        isValidaded = true
                    }
                } else {
                    binding.phoneNumberTextField.error = "     Telephone is required!"
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

        binding.nameTextField.editText?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                isValidaded = false
                if (s != null) {
                    if (s.isEmpty()) {
                        binding.nameTextField.error = "     Name is required!"
                    } else {
                        binding.nameTextField.error = null
                        isValidaded = true
                    }
                } else {
                    binding.nameTextField.error = "     Name is required!"
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

        binding.buttonRegister.setOnClickListener {

            val parent = requireActivity()
            if (parent is MainActivity) {
                if (parent.connectionLiveData.value == false) {
                    Toast.makeText(
                        activity,
                        "You don't have internet connection. Check your internet and try again",
                        Toast.LENGTH_LONG
                    ).show()
                } else {
                    binding.progressbar.visibility = View.VISIBLE

                    binding.nameTextField.error = null
                    binding.phoneNumberTextField.error = null
                    if (isValidaded && binding.birthDateTextView.text != null) {

                        if (binding.editTextEmailAddressSignUp.text.toString().trim()
                                .isNotEmpty() && binding.editTextPasswordSignUp.text.toString().trim()
                                .isNotEmpty()
                        ) {
                            authViewModel.signUp(
                                binding.editTextEmailAddressSignUp.text.toString().trim(),
                                binding.editTextPasswordSignUp.text.toString().trim()
                            )

                        } else {
                            binding.progressbar.visibility = View.GONE
                            Toast.makeText(activity, "Fields cannot be empty", Toast.LENGTH_LONG).show()
                        }

                        userViewModel.updateUserProfile(
                            binding.nameTextField.editText?.text.toString(),
                            binding.birthDateTextView.text.toString(),
                            binding.phoneNumberTextField.editText?.text.toString()
                        )

                    } else {
                        binding.progressbar.visibility = View.GONE
                        Toast.makeText(
                            activity,
                            "Please check your profile information",
                            Toast.LENGTH_LONG
                        )
                            .show()
                    }
                }
            }

        }

    }

}