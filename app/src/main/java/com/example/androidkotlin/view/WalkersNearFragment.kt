package com.example.androidkotlin.view

import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.androidkotlin.R
import com.example.androidkotlin.adapters.MyItemRecyclerViewAdapter
import com.example.androidkotlin.adapters.WalkersNearRecyclerViewAdapter
import com.example.androidkotlin.databinding.FragmentItemListBinding
import com.example.androidkotlin.databinding.FragmentWalkersNearBinding
import com.example.androidkotlin.viewmodel.WalkersViewModel
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.model.LatLng

class WalkersNearFragment : Fragment() {
    private val walkersViewModel: WalkersViewModel by viewModels()
    private val adapter: MyItemRecyclerViewAdapter? = null
    private lateinit var binding: FragmentWalkersNearBinding
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentWalkersNearBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val parent = requireActivity()

        if (parent is MainActivity) {
            val noConnectionMessage = binding.noConnection.noConnection
            val outdatedData = binding.outdatedData.outdatedData
            parent.connectionLiveData.observe(parent) { isNetworkAvailable ->
                isNetworkAvailable.let { isAvailable ->
                    if (!isAvailable && walkersViewModel.walkers.value != null) {
                        outdatedData.visibility = View.VISIBLE
                        noConnectionMessage.visibility = View.GONE

                    } else if (isAvailable) {
                        outdatedData.visibility = View.GONE
                        noConnectionMessage.visibility = View.GONE
                        checkLocationPermission()
                    } else if (!isAvailable && walkersViewModel.walkers.value == null) {
                        noConnectionMessage.visibility = View.VISIBLE
                    }
                }
            }
        }
    }

    private fun checkLocationPermission() {
        val permission = Manifest.permission.ACCESS_FINE_LOCATION
        val granted = ActivityCompat.checkSelfPermission(requireContext(), permission)
        if (granted != PackageManager.PERMISSION_GRANTED) {

            onLocationUnavailable()

            if (ActivityCompat.shouldShowRequestPermissionRationale(requireActivity(), permission)) {
                AlertDialog.Builder(requireContext())
                    .setTitle("Location permission required")
                    .setMessage("This app needs access to your location to enable the weather suggestion service")
                    .setPositiveButton("OK") { _, _ -> requestPermissionLauncher.launch(permission)}
                    .setNegativeButton("Cancel", null)
                    .create()
                    .show()
            }
            requestPermissionLauncher.launch(permission)

        } else {
            onLocationAccessGranted()
        }
    }

    private val requestPermissionLauncher =
        registerForActivityResult(
            ActivityResultContracts.RequestPermission()
        ) { isGranted: Boolean ->
            if (isGranted) {
                onLocationAccessGranted()
            } else {
                onLocationUnavailable()
            }
        }

    private fun onLocationAccessGranted() {
        binding.noLocation.noLocation.visibility = View.GONE
        binding.walkersNearProgressbar.visibility = View.VISIBLE

        try {
            fusedLocationClient.lastLocation.addOnSuccessListener { location ->
                if (location != null) {
                    walkersViewModel.updateCurrentWalkersDataNear(location.latitude, location.longitude, requireContext())

                    with (binding.walkersNearRecyclerview) {
                        walkersViewModel.walkers.observe(viewLifecycleOwner) { currentWalkers ->
                            adapter = WalkersNearRecyclerViewAdapter(currentWalkers)
                            binding.noConnection.noConnection.visibility = View.GONE
                            binding.walkersNearProgressbar.visibility = View.GONE
                            binding.walkersNearRecyclerview.visibility = View.VISIBLE

                            if (currentWalkers.isEmpty()) {
                                binding.walkersEmptyText.visibility = View.VISIBLE
                            }
                            else {
                                binding.walkersEmptyText.visibility = View.GONE
                            }
                        }
                    }
                }
            }
        } catch (e: SecurityException) {
            e.printStackTrace()
        }
    }

    private fun onLocationUnavailable() {
        binding.walkersNearTitle.visibility = View.GONE
        binding.walkersNearRecyclerview.visibility = View.GONE
        binding.noLocation.noLocation.visibility = View.VISIBLE
    }
}