package com.example.androidkotlin.view

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import com.example.androidkotlin.R
import com.example.androidkotlin.databinding.FragmentWalkerProfileBinding
import com.example.androidkotlin.viewmodel.UserViewModel
import com.example.androidkotlin.viewmodel.WalkersViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.launch

class WalkerProfileFragment : Fragment() {
    private var _binding: FragmentWalkerProfileBinding? = null
    private val userViewModel : UserViewModel by viewModels()
    private val walkersViewModel : WalkersViewModel by viewModels()

    private val cityIsValid = MutableStateFlow(false)
    private val neighborhoodIsValid = MutableStateFlow(false)
    private val descriptionIsValid = MutableStateFlow(false)
    private var addressSet = MutableStateFlow(true)

    private val formIsValid = combine(
        cityIsValid,
        neighborhoodIsValid,
        descriptionIsValid,
        addressSet
    ) { cityIsValid, neighborhoodIsValid, descriptionIsValid, addressSet ->
        cityIsValid && neighborhoodIsValid && descriptionIsValid && addressSet
    }

    private lateinit var parentContext: MainActivity

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is MainActivity) {
            parentContext = context
        } else {
            throw RuntimeException("$context attach error")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        walkersViewModel.getWalkerProfile()
    }

    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentWalkerProfileBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.linearLayout.visibility = View.GONE
        binding.progressBar.visibility = View.VISIBLE

        walkersViewModel.walkerProfile.observe(viewLifecycleOwner) { currentWalker ->
            if (currentWalker != null) {
                //binding.nameTextInputLayout.editText?.setText(currentWalker.name)
                binding.cityTextInputLayout.editText?.setText(currentWalker.city)
                binding.neighborhoodTextInputLayout.editText?.setText(currentWalker.neighborhood)
                binding.descriptionTextInputLayout.editText?.setText(currentWalker.description)
                binding.addressText.text = "${currentWalker.radius} m around ${currentWalker.address}"

                //binding.titleText.text = R.string.walker_edit_title.toString()
                //binding.descriptionText.text = R.string.walker_edit_description.toString()
                //binding.submitButton.text = R.string.walker_update_profile.toString()

                binding.progressBar.visibility = View.GONE
                binding.unexpectedError.unexpectedError.visibility = View.GONE
                binding.linearLayout.visibility = View.VISIBLE
            }
            getWalkerProfileFromCache()
        }

        userViewModel.fetchUserProfile()
        userViewModel.userProfile.observe(viewLifecycleOwner) { currentUser ->
            if (currentUser != null) {
                binding.emailTextInputLayout.editText?.setText(currentUser.email)
                binding.nameTextInputLayout.editText?.setText(currentUser.name)

                binding.progressBar.visibility = View.GONE
                binding.unexpectedError.unexpectedError.visibility = View.GONE
                binding.linearLayout.visibility = View.VISIBLE
            }
        }

        userViewModel.error?.observe(viewLifecycleOwner) {
            binding.progressBar.visibility = View.GONE
            binding.linearLayout.visibility = View.GONE
            binding.unexpectedError.unexpectedError.visibility = View.VISIBLE
        }

        binding.addressButton.setOnClickListener {
            saveWalkerProfileToCache()
            findNavController().navigate(R.id.action_walkerProfileFragment_to_walkerProfileAreaFragment)
        }

        binding.cityTextInputLayout.editText?.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                cityIsValid.value = false
                with (binding.cityTextInputLayout) {
                    if (s != null) {
                        if (s.isEmpty()) {
                            error = "City is required"
                        }
                        else if (s.length > 30) {
                            error = "City is too long"
                        }
                        else {
                            error = null
                            cityIsValid.value = true
                        }
                    }
                    else {
                        error = "City is required"
                    }
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })

        binding.neighborhoodTextInputLayout.editText?.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                neighborhoodIsValid.value = false
                with (binding.neighborhoodTextInputLayout) {
                    if (s != null) {
                        if (s.isEmpty()) {
                            error = "Neighborhood is required"
                        }
                        else if (s.length > 30) {
                            error = "Neighborhood is too long"
                        }
                        else {
                            error = null
                            neighborhoodIsValid.value = true
                        }
                    }
                    else {
                        error = "Neighborhood is required"
                    }
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })

        binding.descriptionTextInputLayout.editText?.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                descriptionIsValid.value = false
                with (binding.descriptionTextInputLayout) {
                    if (s != null) {
                        if (s.isEmpty()) {
                            error = "Description is required"
                        }
                        else if (s.length > 300) {
                            error = "Description is too long"
                        }
                        else {
                            error = null
                            descriptionIsValid.value = true
                        }
                    }
                    else {
                        error = "Description is required"
                    }
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })

        binding.submitButton.setOnClickListener {
            with(binding) {
                val temp = walkersViewModel.getWalkerProfileInCache()
                walkersViewModel.updateWalkerProfile(
                    emailTextInputLayout.editText?.text.toString(),
                    nameTextInputLayout.editText?.text.toString(),
                    cityTextInputLayout.editText?.text.toString(),
                    neighborhoodTextInputLayout.editText?.text.toString(),
                    descriptionTextInputLayout.editText?.text.toString(),
                    temp!!.address!!,
                    temp!!.latitude!!,
                    temp!!.longitude!!,
                    temp!!.radius!!
                )
            }
            Toast.makeText(requireContext(), "Profile updated!", Toast.LENGTH_SHORT).show()
            findNavController().navigate(R.id.action_walkerProfileFragment_to_FirstFragment)
        }

        lifecycleScope.launch {
            formIsValid.collect { valid ->
                with (binding.submitButton) {
                    isEnabled = valid
                }
            }
        }
    }

    private fun saveWalkerProfileToCache() {
        with(binding) {
            walkersViewModel.saveWalkerProfileInCache(emailTextInputLayout.editText?.text.toString(),
                                                        nameTextInputLayout.editText?.text.toString(),
                                                        cityTextInputLayout.editText?.text.toString(),
                                                        neighborhoodTextInputLayout.editText?.text.toString(),
                                                        descriptionTextInputLayout.editText?.text.toString(),
                                                        null,
                                                        null,
                                                        null,
                                                        null)
        }
    }

    private fun getWalkerProfileFromCache() {
        val walker = walkersViewModel.getWalkerProfileInCache()
        if (walker.name != "") binding.nameTextInputLayout.editText?.setText(walker.name)
        if (walker.city != "") binding.cityTextInputLayout.editText?.setText(walker.city)
        if (walker.neighborhood != "") binding.neighborhoodTextInputLayout.editText?.setText(walker.neighborhood)
        if (walker.description != "") binding.descriptionTextInputLayout.editText?.setText(walker.description)
        if (walker.radius != 0)
            binding.addressText.text = "${walker.radius} m around ${walker.address}"
    }


}