package com.example.androidkotlin.view

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.androidkotlin.databinding.FragmentPetProfileBinding
import com.example.androidkotlin.viewmodel.PetViewModel
import java.io.ByteArrayOutputStream
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.example.androidkotlin.R
import com.example.androidkotlin.model.EventModel

class PetProfileFragment : Fragment() {
    private var myParentContext: MainActivity? = null
    private val petViewModel : PetViewModel by viewModels()
    private lateinit var binding: FragmentPetProfileBinding
    private var petImage: String = ""
    private var petEvents: MutableList<EventModel> = ArrayList()
    private var petLiked: MutableList<String> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentPetProfileBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        myParentContext = context as MainActivity
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.buttonTakePicture.setOnClickListener {
            val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(takePictureIntent, 69)
        }

        petViewModel.petProfile.observe(viewLifecycleOwner, Observer { currentPet ->
            if(currentPet != null){
                petImage = currentPet.Image.toString()
                binding.nameTextField.setText(currentPet.name)
                binding.emailTextField.setText(currentPet.breed)
                binding.birthDateTextField.setText(currentPet.age)
                binding.weigth.setText(currentPet.weight)
                petEvents = currentPet.Events
                petLiked = currentPet.PetsLiked
            }
            binding.weigthContainer.visibility = View.VISIBLE
            binding.nameContainer.visibility = View.VISIBLE
            binding.emailContainer.visibility = View.VISIBLE
            binding.birthDateContainer.visibility = View.VISIBLE
            binding.buttonSave.visibility = View.VISIBLE
        })

        binding.nameTextField.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                binding.nameContainer.helperText = validName()
            }
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

        binding.emailTextField.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                binding.emailContainer.helperText = validBreed()
            }
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

        binding.birthDateTextField.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                binding.birthDateContainer.helperText = validBirthDate()
            }
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

        binding.weigth.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                binding.weigthContainer.helperText = validWeight()
            }
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

        binding.buttonSave.setOnClickListener{
            binding.progressbar.visibility = View.VISIBLE
            if (binding.nameContainer.helperText == null && binding.emailContainer.helperText == null && binding.birthDateContainer.helperText == null && binding.weigthContainer.helperText == null) {
                petViewModel.updatePetProfile(
                    petImage,
                    binding.birthDateTextField.text.toString(),
                    binding.emailTextField.text.toString(),
                    binding.nameTextField.text.toString(),
                    binding.weigth.text.toString(),
                    petEvents,
                    petLiked
                )
                binding.progressbar.visibility = View.GONE
                val parent = requireActivity()
                if (parent is MainActivity) {
                    if (parent.connectionLiveData.value == false) {
                        Toast.makeText(
                            activity,
                            "Pet profile has been saved locally. Will be sync once you are connected again.",
                            Toast.LENGTH_LONG
                        ).show()
                    } else {
                        Toast.makeText(
                            activity,
                            "Pet profile has been saved successfully",
                            Toast.LENGTH_LONG
                        )
                            .show()
                    }
                }
            }
            else{
                binding.progressbar.visibility = View.GONE
                Toast.makeText(activity,"Please check pet profile information", Toast.LENGTH_LONG).show()
            }
        }

        binding.buttonSeeDoc.setOnClickListener {
            findNavController().navigate(R.id.action_petProfileFragment_to_petDocumentFragment)
        }



        val parent = requireActivity()

        if (parent is MainActivity) {
            val noConnectionMessage = binding.noConnection.noConnection
            val outdatedData = binding.outdatedData.outdatedData
            val save = binding.buttonSave
            val picture = binding.circle
            parent.connectionLiveData.observe(parent) { isNetworkAvailable ->
                isNetworkAvailable.let { isAvailable ->
                    if (!isAvailable && petViewModel.petProfile.value != null) {
                        outdatedData.visibility = View.VISIBLE
                        noConnectionMessage.visibility = View.GONE

                    } else if(isAvailable){
                        outdatedData.visibility = View.GONE
                        noConnectionMessage.visibility = View.GONE
                        save.visibility = View.VISIBLE
                        picture.visibility = View.VISIBLE
                        petViewModel.fetchPetProfile()
                    } else if(!isAvailable && petViewModel.petProfile.value == null){
                        noConnectionMessage.visibility = View.VISIBLE
                        save.visibility = View.GONE
                        picture.visibility = View.GONE
                    }
                }
            }
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == 69 && resultCode == Activity.RESULT_OK)
        {
            val takenImage = data?.extras?.get("data") as Bitmap
            val stream = ByteArrayOutputStream()
            takenImage.compress(Bitmap.CompressFormat.PNG, 100, stream)
            val imageEncoded: String = Base64.encodeToString(stream.toByteArray(), Base64.DEFAULT)
            petViewModel.updatePetImage(imageEncoded)
        }
    }

    private fun validName(): String?{
        val nameText = binding.nameTextField.text.toString()
        if(nameText.isNullOrEmpty()){
            return "You have to provide a name to your pet"
        }
        if (nameText.length > 50){
            return "Pet name is too long"
        }
        return null
    }

    private fun validBreed(): String?{
        val breedText = binding.nameTextField.text.toString()
        if(breedText.isNullOrEmpty()){
            return "You have to provide a breed to your pet"
        }
        if (breedText.length > 50){
            return "Pet breed is too long"
        }
        return null
    }

    private fun validBirthDate(): String?{
        val ageText = binding.birthDateTextField.text.toString()
        if(ageText.isNullOrEmpty()){
            return "You have to provide your pets age"
        }
        if(ageText.toLong() < 0 || ageText.toLong() > 30){
            return "Pet age out of range"
        }
        return null
    }

    private fun validWeight(): String?{
        val weightText = binding.weigth.text.toString()
        if(weightText.isNullOrEmpty()){
            return "You have to provide your pets weight"
        }
        if(weightText.toDouble() < 0 || weightText.toDouble() > 100){
            return "Pet weight out of range"
        }
        return null
    }
}