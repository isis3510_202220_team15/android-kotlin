package com.example.androidkotlin.view

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.example.androidkotlin.databinding.FragmentEventBinding
import com.example.androidkotlin.model.EventModel
import com.example.androidkotlin.viewmodel.PetViewModel
import com.google.android.material.datepicker.CalendarConstraints
import com.google.android.material.datepicker.DateValidatorPointForward
import com.google.android.material.datepicker.MaterialDatePicker
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class EventFragment : Fragment() {
    private lateinit var binding: FragmentEventBinding
    private val eventViewModel : PetViewModel by viewModels()
    private val petViewModel : PetViewModel by viewModels()
    private var petEvents: MutableList<EventModel> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentEventBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.eventDateTextView.setOnClickListener {
            Locale.setDefault(Locale.ENGLISH)
            val constraintsBuilder = CalendarConstraints.Builder().setValidator(DateValidatorPointForward.now())
            val datePicker = MaterialDatePicker.Builder.datePicker().setCalendarConstraints(constraintsBuilder.build()).setTitleText("Select Event Date").build()
            activity?.let { it1 -> datePicker.show(it1.supportFragmentManager, "DatePicker") }
            datePicker.addOnPositiveButtonClickListener { date ->
                val dateFormatter = SimpleDateFormat("dd-MM-yyyy")
                val formattedDate = dateFormatter.format(Date(date))
                binding.eventDateTextView.text = formattedDate
            }
        }

        petViewModel.petProfile.observe(viewLifecycleOwner, Observer { currentPet ->
            if(currentPet != null){
                petEvents = currentPet.Events
            }
        })

        binding.eventNameText.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(s: Editable?) {
                binding.eventNameTextContent.helperText = validName()
            }
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

        binding.eventDateTextView.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(s: Editable?) {
                binding.eventDateTextContent.helperText = validDate()
            }
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

        binding.eventDescriptionText.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(s: Editable?) {
                binding.eventDescriptionTextView.helperText = validDesc()
            }
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

        binding.buttonSave.setOnClickListener{
            if (binding.eventNameTextContent.helperText == null && binding.eventDateTextContent.helperText == null && binding.eventDescriptionTextView.helperText == null) {
                val mod = EventModel(binding.eventNameText.text.toString(), binding.eventDateTextView.text.toString(), binding.eventDescriptionText.text.toString())
                val even = petEvents
                even.add(mod)
                eventViewModel.createEventProfile(even)
                Toast.makeText(activity, "Event added successfully", Toast.LENGTH_LONG).show()
                binding.eventNameText.setText("")
                binding.eventDateTextView.text = ""
                binding.eventDescriptionText.setText("")
                binding.eventNameTextContent.helperText = null
                binding.eventDateTextContent.helperText = null
                binding.eventDescriptionTextView.helperText = null
            }
            else{
                Toast.makeText(activity,"Please check the event information", Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun validName(): String?{
        val nameText = binding.eventNameText.text.toString()
        if(nameText.isNullOrEmpty()){
            return "You have to provide a name your pet's event"
        }
        if (nameText.length > 50){
            return "Event name is too long"
        }
        return null
    }

    private fun validDate(): String?{
        val nameText = binding.eventNameText.text.toString()
        if(nameText.isNullOrEmpty()){
            return "You have to provide a date of your pet's event"
        }
        return null
    }

    private fun validDesc(): String?{
        val nameText = binding.eventNameText.text.toString()
        if(nameText.isNullOrEmpty()){
            return "You have to provide a name your pet's description event"
        }
        if (nameText.length > 50){
            return "Event description is too long"
        }
        return null
    }
}