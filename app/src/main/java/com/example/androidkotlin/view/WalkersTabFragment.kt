package com.example.androidkotlin.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager2.widget.ViewPager2
import com.example.androidkotlin.R
import com.example.androidkotlin.adapters.WalkersTabPageAdapter
import com.example.androidkotlin.databinding.FragmentFirstBinding
import com.example.androidkotlin.databinding.FragmentWalkersTabBinding
import com.google.android.material.tabs.TabLayout

/**
 * A simple [Fragment] subclass.
 * Use the [WalkersTabFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class WalkersTabFragment : Fragment() {
    private var _binding: FragmentWalkersTabBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentWalkersTabBinding.inflate(inflater, container, false)
        setUpTabBar()
        return binding.root
    }

    private fun setUpTabBar() {
        val adapter = WalkersTabPageAdapter(requireActivity(), binding.tabLayout.tabCount)
        binding.viewPager2.adapter = adapter

        binding.viewPager2.registerOnPageChangeCallback(object: ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                binding.tabLayout.selectTab(binding.tabLayout.getTabAt(position))
            }
        })

        binding.tabLayout.addOnTabSelectedListener(object: TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                binding.viewPager2.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {}

            override fun onTabReselected(tab: TabLayout.Tab?) {}
        })
    }
}