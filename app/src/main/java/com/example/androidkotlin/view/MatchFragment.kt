package com.example.androidkotlin.view

import android.app.Application
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.example.androidkotlin.R
import com.example.androidkotlin.databinding.FragmentMatchBinding
import com.example.androidkotlin.databinding.FragmentTinderBinding
import com.example.androidkotlin.db.DBHelper
import com.example.androidkotlin.model.PetModel
import com.example.androidkotlin.model.PlaceModel
import com.example.androidkotlin.viewmodel.PetViewModel
import com.google.firebase.functions.FirebaseFunctions
import com.google.firebase.functions.ktx.functions
import com.google.firebase.ktx.Firebase
import java.util.ArrayList

class MatchFragment : Fragment() {
    private val petViewModel : PetViewModel by viewModels()
    private lateinit var parentContext: MainActivity
    private lateinit var binding: FragmentMatchBinding
    private lateinit var functions: FirebaseFunctions
    private var pets : MutableList<String> = ArrayList()
    private var actualPet : PetModel = PetModel()

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is MainActivity) {
            parentContext = context
        } else {
            throw RuntimeException("$context attach error")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        functions = Firebase.functions
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMatchBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        petViewModel.getPets()

        petViewModel.petProfile.observe(viewLifecycleOwner, Observer { currentPet ->
            actualPet = currentPet
        })

        val db = DBHelper(parentContext, null)

        parentContext.connectionLiveData.observe(viewLifecycleOwner) { isNetworkAvailable ->
            isNetworkAvailable.let { isAvalilable ->
                if (!isAvalilable){
                    val cursor = db.getMatch()
                    pets = arrayListOf()
                    with(cursor) {
                        while (this?.moveToNext() == true) {
                            val pet =  getString(getColumnIndexOrThrow("RESP"))
                            pets.add(pet)
                        }
                    }
                    val arrayAdapter: ArrayAdapter<*>
                    if (pets.size <= 0){
                        pets.add("There are no Matches")
                        arrayAdapter = ArrayAdapter(parentContext, android.R.layout.simple_list_item_1, pets)
                        binding.matchList.adapter = arrayAdapter
                    } else{
                        arrayAdapter = ArrayAdapter(parentContext, android.R.layout.simple_list_item_1, pets)
                        binding.matchList.adapter = arrayAdapter
                    }
                    cursor?.close()
                }
                else {
                    petViewModel.petsData.observe(viewLifecycleOwner, Observer { currentPet ->
                        pets = arrayListOf()
                        if(currentPet.size > 0){

                            db.resetDB()
                            for(pet in currentPet){
                                if(actualPet.PetsLiked.contains(pet.userId) && pet.PetsLiked.contains(actualPet.userId)){
                                    pets.add("Match with " + pet.name + " age: " + pet.age + " breed: " + pet.breed + " you can contact his owner by email: " + pet.userId)
                                    db.addMatch(pet.userId.toString(), "Match with " + pet.name + " age: " + pet.age + " breed: " + pet.breed + " you can contact his owner by email: " + pet.userId)
                                }
                            }
                            val arrayAdapter: ArrayAdapter<*>
                            if (pets.size <= 0){
                                pets.add("There are no Matches")
                                arrayAdapter = ArrayAdapter(parentContext, android.R.layout.simple_list_item_1, pets)
                                binding.matchList.adapter = arrayAdapter
                            } else{
                                arrayAdapter = ArrayAdapter(parentContext, android.R.layout.simple_list_item_1, pets)
                                binding.matchList.adapter = arrayAdapter
                            }
                        }
                    })
                }
            }
        }
    }
}