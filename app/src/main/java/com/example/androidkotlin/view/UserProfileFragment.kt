package com.example.androidkotlin.view

import android.os.Bundle
import android.telephony.PhoneNumberUtils
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.example.androidkotlin.databinding.FragmentUserProfileBinding
import com.example.androidkotlin.viewmodel.UserViewModel
import com.google.android.material.datepicker.CalendarConstraints
import com.google.android.material.datepicker.DateValidatorPointBackward
import com.google.android.material.datepicker.MaterialDatePicker
import java.text.SimpleDateFormat
import java.util.*

class UserProfileFragment : Fragment() {

    private lateinit var binding: FragmentUserProfileBinding
    private val userViewModel: UserViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentUserProfileBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {



            userViewModel.userProfile.observe(viewLifecycleOwner, Observer { currentUser ->
                if (currentUser != null) {
                    binding.emailTextField.text = currentUser.email
                    binding.nameTextField.editText?.setText(currentUser.name)
                    binding.phoneNumberTextField.editText?.setText(currentUser.phoneNumber)
                    binding.birthDateTextView.text = currentUser.birthDate
                }
                binding.emailTextField.visibility = View.VISIBLE
                binding.birthDateTextView.visibility = View.VISIBLE
                binding.nameTextField.visibility = View.VISIBLE
                binding.phoneNumberTextField.visibility = View.VISIBLE
                binding.buttonSave.visibility = View.VISIBLE
            })

        userViewModel.error?.observe(viewLifecycleOwner, Observer { error ->
            Toast.makeText(activity, error.toString(), Toast.LENGTH_LONG
            ).show()
        })

        binding.birthDateTextView.setOnClickListener {
            Locale.setDefault(Locale.ENGLISH)
            val constraintsBuilder =
                CalendarConstraints.Builder().setValidator(DateValidatorPointBackward.now())
            val datePicker = MaterialDatePicker.Builder.datePicker()
                .setCalendarConstraints(constraintsBuilder.build())
                .setTitleText("Select date of birth").build()
            activity?.let { it1 -> datePicker.show(it1.supportFragmentManager, "DatePicker") }

            // Setting up the event for when ok is clicked
            datePicker.addOnPositiveButtonClickListener { date ->
                val dateFormatter = SimpleDateFormat("dd-MM-yyyy")
                val formattedDate = dateFormatter.format(Date(date))
                binding.birthDateTextView.text = formattedDate
            }
        }

        var isValidaded = false

        binding.phoneNumberTextField.editText?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                isValidaded = false
                if (s != null) {
                    if (s.isEmpty()) {
                        binding.phoneNumberTextField.error = "     Telephone is required!"
                    } else if (s.length in 0..9) {
                        binding.phoneNumberTextField.error = "     Incorrect telephone format"
                    } else if (!PhoneNumberUtils.isGlobalPhoneNumber(s.toString())) {
                        binding.phoneNumberTextField.error = "     Incorrect telephone format"
                    } else {
                        binding.phoneNumberTextField.error = null
                        isValidaded = true
                    }
                } else {
                    binding.phoneNumberTextField.error = "     Telephone is required!"
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

        binding.nameTextField.editText?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                isValidaded = false
                if (s != null) {
                    if (s.isEmpty()) {
                        binding.nameTextField.error = "     Name is required!"
                    } else {
                        binding.nameTextField.error = null
                        isValidaded = true
                    }
                } else {
                    binding.nameTextField.error = "     Name is required!"
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

        binding.buttonSave.setOnClickListener {
            binding.progressbar.visibility = View.VISIBLE
            binding.nameTextField.error = null
            binding.phoneNumberTextField.error = null
            if (isValidaded) {
                userViewModel.updateUserProfile(
                    binding.nameTextField.editText?.text.toString(),
                    binding.birthDateTextView.text.toString(),
                    binding.phoneNumberTextField.editText?.text.toString()
                )
                val parent = requireActivity()
                if (parent is MainActivity) {
                    if (parent.connectionLiveData.value == false) {
                        Toast.makeText(
                            activity,
                            "Your profile information has been saved locally. Will be sync once you are connected again",
                            Toast.LENGTH_LONG
                        ).show()
                    } else {
                        Toast.makeText(
                            activity,
                            "Your profile information has been saved",
                            Toast.LENGTH_LONG
                        )
                            .show()
                    }
                }
                binding.progressbar.visibility = View.GONE
            } else {
                binding.progressbar.visibility = View.GONE
                Toast.makeText(
                    activity,
                    "Please check your profile information",
                    Toast.LENGTH_LONG
                )
                    .show()
            }
        }


        val parent = requireActivity()

        if (parent is MainActivity) {
            val noConnectionMessage = binding.noConnection.noConnection
            val outdatedData = binding.outdatedData.outdatedData
            val save = binding.buttonSave
            val picture = binding.circle
            parent.connectionLiveData.observe(parent) { isNetworkAvailable ->
                isNetworkAvailable.let { isAvailable ->
                    if (!isAvailable && userViewModel.userProfile.value != null) {
                        outdatedData.visibility = View.VISIBLE
                        noConnectionMessage.visibility = View.GONE

                    } else if(isAvailable){
                        outdatedData.visibility = View.GONE
                        noConnectionMessage.visibility = View.GONE
                        save.visibility = View.VISIBLE
                        picture.visibility = View.VISIBLE
                        userViewModel.fetchUserProfile()
                    } else if(!isAvailable && userViewModel.userProfile.value == null){
                        noConnectionMessage.visibility = View.VISIBLE
                        save.visibility = View.GONE
                        picture.visibility = View.GONE
                    }
                }
            }
        }

    }

}
