package com.example.androidkotlin.view

import android.app.Activity
import android.content.Context
import android.content.Context.INPUT_METHOD_SERVICE
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.SearchView
import android.widget.Toast
import androidx.core.content.ContextCompat.getSystemService
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.androidkotlin.adapters.MyItemRecyclerViewAdapter
import com.example.androidkotlin.databinding.FragmentWalkersSearchBinding
import com.example.androidkotlin.viewmodel.WalkersViewModel

class WalkersSearchFragment : Fragment() {
    private var columnCount = 1
    private val walkersViewModel: WalkersViewModel by viewModels()
    private val adapter: MyItemRecyclerViewAdapter? = null
    private lateinit var binding: FragmentWalkersSearchBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            columnCount = it.getInt(WalkersListFragment.ARG_COLUMN_COUNT)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentWalkersSearchBinding.inflate(inflater, container, false)
        val view = binding.walkersSearchRecyclerview
        // Set the adapter
        with(view) {
            layoutManager = when {
                columnCount <= 1 -> LinearLayoutManager(context)
                else -> GridLayoutManager(context, columnCount)
            }

            walkersViewModel.walkers.observe(viewLifecycleOwner) { currentWalkers ->
                adapter = MyItemRecyclerViewAdapter(currentWalkers, walkersViewModel)
                val noConnectionMessage = binding.noConnection.noConnection
                noConnectionMessage.visibility = View.GONE
                if (currentWalkers.isEmpty()) {
                    noConnectionMessage.visibility = View.VISIBLE
                }
            }
        }

        binding.walkersSearchview.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    if (!query.isNullOrBlank()) {
                        walkersViewModel.updateCurrentWalkersDataBySearch("neighborhood", query)
                        binding.walkersSearchRecyclerview.adapter!!.notifyDataSetChanged()
                        hideKeyboard()
                    }
                    return true
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    if (newText.isNullOrBlank()) {
                        walkersViewModel.updateCurrentWalkersData()
                        binding.walkersSearchRecyclerview.adapter?.notifyDataSetChanged()
                        hideKeyboard()
                    }
                    return true
                }
            }
        )

        //return view
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val parent = requireActivity()

        if (parent is MainActivity) {
            val noConnectionMessage = binding.noConnection.noConnection
            val outdatedData = binding.outdatedData.outdatedData
            val search = binding.searchFiltersLinearlayout
            parent.connectionLiveData.observe(parent) { isNetworkAvailable ->
                isNetworkAvailable.let { isAvailable ->
                    if (!isAvailable && walkersViewModel.walkers.value != null) {
                        outdatedData.visibility = View.VISIBLE
                        noConnectionMessage.visibility = View.GONE
                        search.visibility = View.GONE

                    } else if(isAvailable){
                        outdatedData.visibility = View.GONE
                        noConnectionMessage.visibility = View.GONE
                        search.visibility = View.VISIBLE
                        walkersViewModel.updateCurrentWalkersData()
                    } else if(!isAvailable && walkersViewModel.walkers.value == null){
                        noConnectionMessage.visibility = View.VISIBLE
                        search.visibility = View.GONE
                    }
                }
            }
        }
    }

    // Hide the keyboard
    fun Fragment.hideKeyboard() {
        view?.let { activity?.hideKeyboard(it) }
    }

    fun Activity.hideKeyboard() {
        hideKeyboard(currentFocus ?: View(this))
    }

    private fun Context.hideKeyboard(view: View) {
        val inputMethodManager =
            getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }
}