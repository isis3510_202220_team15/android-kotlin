package com.example.androidkotlin.view

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.example.androidkotlin.R
import com.example.androidkotlin.databinding.ActivityMainBinding
import com.example.androidkotlin.network.ConnectionLiveData
import com.example.androidkotlin.network.TAG
import com.example.androidkotlin.viewmodel.PetViewModel
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import io.sentry.Sentry


class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityMainBinding
    lateinit var connectionLiveData: ConnectionLiveData
    private val petViewModel : PetViewModel by viewModels()
    lateinit var snackBar: Snackbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Sentry.captureMessage("testing SDK setup")
        try {
            throw Exception("This is a test.")
        } catch (e: Exception) {
            Sentry.captureException(e)
        }
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        snackBar = Snackbar.make(
            binding.root,
            "No internet connection",
            Snackbar.LENGTH_INDEFINITE).addCallback(object : BaseTransientBottomBar.BaseCallback<Snackbar>() {
            override fun onShown(transientBottomBar: Snackbar?) {
                super.onShown(transientBottomBar)
            }

            override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                super.onDismissed(transientBottomBar, event)
                Snackbar.make(
                    binding.root,
                    "You are now connected to the internet",
                    Snackbar.LENGTH_LONG).show()
            }
        })

        connectionLiveData = ConnectionLiveData(this)
        //connectionLiveData.initialize()
        connectionLiveData.observe(this) { isNetworkAvailable ->
            Log.d(TAG, "isNetworkAvailable: $isNetworkAvailable")
            isNetworkAvailable.let { isAvalilable ->
                if (!isAvalilable){
                    snackBar.show()
                }
                else {
                    snackBar.dismiss()
                }
            }
        }

        setSupportActionBar(binding.toolbar)

        val navigationHost =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        val navController = navigationHost.navController

        navController.addOnDestinationChangedListener { _, destination, _ ->
            if (destination.id == R.id.signInFragment) {
                binding.toolbar.visibility = View.GONE
            } else if (destination.id == R.id.userProfileFragment){
                binding.toolbar.visibility = View.GONE
            }
            else if (destination.id == R.id.petProfileFragment){
                binding.toolbar.visibility = View.GONE
            }
            else if (destination.id == R.id.eventFragment){
                binding.toolbar.visibility = View.GONE
            }
            else if (destination.id == R.id.matchFragment){
                binding.toolbar.visibility = View.GONE
            }
            else if (destination.id == R.id.petDocumentFragment){
                binding.toolbar.visibility = View.GONE
            }
            else if (destination.id == R.id.tinderFragment){
                binding.toolbar.visibility = View.GONE
            }
            else if (destination.id == R.id.walkersTabFragment){
                binding.toolbar.visibility = View.VISIBLE
            }
            else if (destination.id == R.id.walkerProfileFragment){
                binding.toolbar.visibility = View.VISIBLE
            }
            else {
                binding.toolbar.visibility = View.VISIBLE
            }
        }

        appBarConfiguration = AppBarConfiguration(
            setOf(R.id.FirstFragment, R.id.signInFragment),
            binding.drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)

        binding.navView.setupWithNavController(navController)

        binding.navView.setNavigationItemSelectedListener { item ->
            when(item.itemId){
                R.id.nav_item_two -> {
                    findNavController(R.id.nav_host_fragment).navigate(R.id.action_FirstFragment_to_userProfileFragment)
                    binding.drawerLayout.closeDrawers()
                }
                R.id.nav_item_three -> {
                    findNavController(R.id.nav_host_fragment).navigate(R.id.action_FirstFragment_to_itemFragment)
                    binding.drawerLayout.closeDrawers()
                }
                R.id.nav_item_match -> {
                    petViewModel.petProfile.observe(this, Observer { currentPet ->
                        if(currentPet.name != ""){
                            findNavController(R.id.nav_host_fragment).navigate(R.id.action_FirstFragment_to_tinderFragment)
                            binding.drawerLayout.closeDrawers()
                        }
                        else{
                            Toast.makeText(this,"Debe crear una mascota primero", Toast.LENGTH_LONG).show()
                        }
                    })
                }
                R.id.nav_item_four -> {
                    findNavController(R.id.nav_host_fragment).navigate(R.id.action_FirstFragment_to_walkerProfileFragment)
                    binding.drawerLayout.closeDrawers()
                }
                R.id.nav_item_five -> {
                    findNavController(R.id.nav_host_fragment).navigate(R.id.action_FirstFragment_to_placesTabFragment)
                    binding.drawerLayout.closeDrawers()
                }
            }
            true
        }

    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration)
                || super.onSupportNavigateUp()
    }
}