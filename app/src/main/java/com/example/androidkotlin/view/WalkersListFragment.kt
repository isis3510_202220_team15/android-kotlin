package com.example.androidkotlin.view

import android.location.Location
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.androidkotlin.adapters.MyItemRecyclerViewAdapter
import com.example.androidkotlin.databinding.FragmentItemListBinding
import com.example.androidkotlin.db.DBHelper
import com.example.androidkotlin.model.PlaceModel
import com.example.androidkotlin.viewmodel.WalkersViewModel


/**
 * A fragment representing a list of Walkers.
 */
class WalkersListFragment() : Fragment() {

    private var columnCount = 1
    private val walkersViewModel: WalkersViewModel by viewModels()
    private val adapter: MyItemRecyclerViewAdapter? = null
    private lateinit var binding: FragmentItemListBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            columnCount = it.getInt(ARG_COLUMN_COUNT)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentItemListBinding.inflate(inflater, container, false)
        val view = binding.walkersSearchRecyclerview
        // Set the adapter
        with(view) {
            layoutManager = when {
                columnCount <= 1 -> LinearLayoutManager(context)
                else -> GridLayoutManager(context, columnCount)
            }

            walkersViewModel.walkers.observe(viewLifecycleOwner) { currentWalkers ->
                adapter = MyItemRecyclerViewAdapter(currentWalkers, walkersViewModel)
                val noConnectionMessage = binding.noConnection.noConnection
                noConnectionMessage.visibility = View.GONE
                if (currentWalkers.isEmpty()) {
                    noConnectionMessage.visibility = View.VISIBLE
                }
            }
        }
        //return view
        return binding.root
    }

    companion object {


        const val ARG_COLUMN_COUNT = "column-count"


        @JvmStatic
        fun newInstance(columnCount: Int) =
            WalkersListFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_COLUMN_COUNT, columnCount)
                }
            }
    }
}