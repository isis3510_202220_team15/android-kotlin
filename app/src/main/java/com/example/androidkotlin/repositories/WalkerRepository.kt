package com.example.androidkotlin.repositories

import android.content.Context
import android.util.Log
import android.util.LruCache
import androidx.lifecycle.coroutineScope
import com.android.volley.RequestQueue
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.example.androidkotlin.model.PlaceModel
import com.example.androidkotlin.model.WalkerModel
import com.example.androidkotlin.network.ConnectionLiveData
import com.example.androidkotlin.services.FirebaseService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import org.json.JSONArray
import org.json.JSONObject

class WalkerRepository() {

    /*var connectionLiveData: ConnectionLiveData

    init {
        connectionLiveData = connection
    }*/

    suspend fun updateCurrentWalkersData(): List<WalkerModel> {
        return FirebaseService.getCurrentWalkersData()
    }

    suspend fun updateCurrentWalkersDataBySearch(field: String, value: String): List<WalkerModel> {
        return FirebaseService.getWalkersDataByStringField(field, value)
    }

    suspend fun updateCurrentWalkersDataSorted(sortedBy: String, ascending: Boolean, limit: Int): List<WalkerModel> {
        //viewModelScope.launch { _walkersModel.value = FirebaseService.getWalkersDataSorted(sortedBy, ascending)}
        val walkerList = FirebaseService.getCurrentWalkersData()
        return walkerList.sortedWith(compareBy {it.likes.size}).reversed().subList(0, limit)
    }

    suspend fun updateCurrentWalkersDataNear(lat: Double, lon: Double, context: Context, onSuccess: (List<WalkerModel>) -> Unit) {
        val walkers = mutableListOf<WalkerModel>()

        val queue: RequestQueue = Volley.newRequestQueue(context)
        val url = "https://us-central1-moviles2022-1973a.cloudfunctions.net/findWalkersNearBy?lat=${lat}&lng=${lon}"

        val request = object: JsonObjectRequest(Method.POST, url, null, { response ->
            val walkersArray: JSONArray = response.getJSONArray("walkers")

            for (i in 0 until walkersArray.length()) {
                val walker = walkersArray.getJSONObject(i)
                walkers.add(
                    WalkerModel(
                        documentId = walker.getString("email"),
                        name = walker.getString("name"),
                        city = walker.getString("city"),
                        neighborhood = walker.getString("neighborhood"),
                        description = walker.getString("description"),
                        latitude = walker.getDouble("latitude").toFloat(),
                        longitude = walker.getDouble("longitude").toFloat(),
                        radius = walker.getInt("radius")
                    )
                )
            }
            onSuccess(walkers.toList())
        }, { error ->
            Log.e("TAG", "ERROR RESPONSE IS $error")
        }){}
        queue.add(request)
    }

    suspend fun handleLikeButton(documentId: String){
        FirebaseService.incrementWalkerLikes(documentId)
    }

    suspend fun handleDislikeButton(documentId: String){
        FirebaseService.decrementWalkerLikes(documentId)
    }

    suspend fun updateWalkerProfile(email: String, name: String, city: String, neighborhood: String, description: String, address: String, lat: Float, lon: Float, radius: Int) {
        FirebaseService.updateWalkerProfile(email, name, city, neighborhood, description, address, lat, lon, radius)
        saveWalkerProfileInCache(email, name, city, neighborhood, description, address, lat, lon, radius)
    }

    suspend fun getWalkerProfile(): WalkerModel? {
        //return if (connectionLiveData.value == true) {
            return FirebaseService.getWalkerProfile()
        //} else {
            //getWalkerProfileFromCache()
        //}

    }

    fun saveWalkerProfileInCache(email: String?, name: String?, city: String?, neighborhood: String?, description: String?, address: String?, lat: Float?, lon: Float?, radius: Int?) {
        saveInCache("email", email)
        saveInCache("name", name)
        saveInCache("city", city)
        saveInCache("neighborhood", neighborhood)
        saveInCache("description", description)
        saveInCache("address", address)
        saveInCache("lat", lat)
        saveInCache("lon", lon)
        saveInCache("radius", radius)
    }

    fun getWalkerProfileFromCache(): WalkerModel {
        val name = if (getFromCache("name") == null) "" else getFromCache("name").toString()
        val email = if (getFromCache("email") == null) "" else getFromCache("email").toString()
        val city = if (getFromCache("city") == null) "" else getFromCache("city").toString()
        val neighborhood = if (getFromCache("neighborhood") == null) "" else getFromCache("neighborhood").toString()
        val description = if (getFromCache("description") == null) "" else getFromCache("description").toString()
        val address = if (getFromCache("address") == null) "" else getFromCache("address").toString()
        val lat : Float = if (getFromCache("lat") == null) 0f else getFromCache("lat") as Float
        val lon : Float = if (getFromCache("lon") == null) 0f else getFromCache("lon") as Float
        val radius : Int = if (getFromCache("radius") == null) 0 else getFromCache("radius") as Int

        return WalkerModel(email, name, ArrayList(), description, city, neighborhood, address, lat, lon, radius)
    }

    companion object {
        private val cache: LruCache<Any, Any> = LruCache(1024)

        fun saveInCache(key: Any, value: Any?) : Boolean {
            try {
                if (value != null)
                    cache.put(key, value)
                return true
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return false
        }

        fun getFromCache(key: Any): Any? {
            return cache.get(key)
        }
    }
}