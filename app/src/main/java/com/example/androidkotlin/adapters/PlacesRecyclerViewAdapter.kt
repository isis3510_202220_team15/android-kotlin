package com.example.androidkotlin.adapters


import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.androidkotlin.databinding.FragmentPlaceItemBinding
import com.example.androidkotlin.model.PlaceModel

class PlacesRecyclerViewAdapter(
    values: List<PlaceModel>,
) : RecyclerView.Adapter<PlacesRecyclerViewAdapter.ViewHolder>() {

    private var places: MutableList<PlaceModel> = values.toMutableList()
    private lateinit var binding: FragmentPlaceItemBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        binding = FragmentPlaceItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )

        return ViewHolder(binding)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = places[position]
        holder.idView.text = item.description
        holder.contentView.text = item.name
        holder.score.text = item.score
        holder.type.text = item.type
        holder.address.text = item.address
        holder.distance.text = String.format("%.2f", item.distance.toDouble())
    }


    override fun getItemCount(): Int = places.size

    inner class ViewHolder(binding: FragmentPlaceItemBinding) : RecyclerView.ViewHolder(binding.root) {
        val idView: TextView = binding.itemNumber
        val contentView: TextView = binding.content
        val score: TextView = binding.score
        val type: TextView = binding.type
        val address: TextView = binding.address
        val distance: TextView = binding.distance

        override fun toString(): String {
            return super.toString() + " '" + contentView.text + "'"
        }
    }

}