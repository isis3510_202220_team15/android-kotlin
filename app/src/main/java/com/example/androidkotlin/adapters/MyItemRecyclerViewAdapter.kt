package com.example.androidkotlin.adapters


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.androidkotlin.databinding.FragmentItemBinding
import com.example.androidkotlin.model.WalkerModel
import com.example.androidkotlin.services.FirebaseService
import com.example.androidkotlin.viewmodel.WalkersViewModel

class MyItemRecyclerViewAdapter(
    values: List<WalkerModel>, private val walkersViewModel: WalkersViewModel,
) : RecyclerView.Adapter<MyItemRecyclerViewAdapter.ViewHolder>() {

    private var walkers: MutableList<WalkerModel> = values.toMutableList()
    private lateinit var binding: FragmentItemBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        binding = FragmentItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )

        return ViewHolder(binding)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = walkers[position]
        holder.idView.text = item.description
        holder.contentView.text = item.name
        holder.likesCounter.text = item.likes.size.toString()

        val likeButton = binding.likeButton
        val dislikeButton = binding.dislikeButton

        if(item.likes.contains(FirebaseService.getCurrentUserEmail())){
            likeButton.visibility = View.GONE
            dislikeButton.visibility = View.VISIBLE
        } else{
            dislikeButton.visibility = View.GONE
            likeButton.visibility = View.VISIBLE
        }

        likeButton.setOnClickListener {
            likeButton.visibility = View.GONE
            dislikeButton.visibility = View.VISIBLE
            walkersViewModel.handleLikeButton(item.documentId)
            item.likes.add(FirebaseService.getCurrentUserEmail())
            walkers[position] = item
            holder.likesCounter.text = item.likes.size.toString()
        }

        dislikeButton.setOnClickListener {
            dislikeButton.visibility = View.GONE
            likeButton.visibility = View.VISIBLE
            walkersViewModel.handleDislikeButton(item.documentId)
            item.likes.remove(FirebaseService.getCurrentUserEmail())
            walkers[position] = item
            holder.likesCounter.text = item.likes.size.toString()
        }

    }


    override fun getItemCount(): Int = walkers.size

    inner class ViewHolder(binding: FragmentItemBinding) : RecyclerView.ViewHolder(binding.root) {
        val idView: TextView = binding.itemNumber
        val contentView: TextView = binding.content
        val likesCounter: TextView = binding.likesCounter

        override fun toString(): String {
            return super.toString() + " '" + contentView.text + "'"
        }
    }

}