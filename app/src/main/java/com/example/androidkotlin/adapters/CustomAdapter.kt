package com.example.androidkotlin.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.androidkotlin.R

class CustomAdapter: RecyclerView.Adapter<CustomAdapter.ViewHolder>(){

    var meals = arrayOf("Next meal in 1 hour", "Next meal in 4 hours", "Next meal in 1 hour", "Next meal in 5 hours", "Next meal in 3 hours")
    var appointments = arrayOf("Next appointment in 3 days", "Next appointment in 14 days", "Next appointment in 5 days", "Next meal in 5 hours", "Next appointment in 36 days")
    var images = arrayOf(
        R.drawable.petimage,
        R.drawable.petimage,
        R.drawable.petimage,
        R.drawable.petimage,
        R.drawable.petimage
    )

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        var v = LayoutInflater.from(viewGroup.context).inflate(R.layout.card_layout, viewGroup, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {
        viewHolder.itemImage.setImageResource(images[i])
        viewHolder.itemAppointment.text = appointments[i]
    }

    override fun getItemCount(): Int {
        return meals.size
    }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        var itemImage: ImageView
        var itemAppointment: TextView

        init {
            itemImage = itemView.findViewById(R.id.item_image)
            itemAppointment = itemView.findViewById(R.id.appointment_text)
        }
    }


}