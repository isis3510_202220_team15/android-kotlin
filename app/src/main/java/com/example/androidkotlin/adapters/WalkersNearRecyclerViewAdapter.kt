package com.example.androidkotlin.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.androidkotlin.R
import com.example.androidkotlin.model.WalkerModel

class WalkersNearRecyclerViewAdapter (private val walkers: List<WalkerModel>) : RecyclerView.Adapter<WalkersNearRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context).inflate(R.layout.fragment_walkers_near_card, parent, false)
        return ViewHolder(inflater)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = walkers[position]
        holder.textName.text = item.name
        holder.textDescription.text = item.description
        holder.textEmail.text = "Contact me: ${item.documentId}"
    }


    override fun getItemCount(): Int = walkers.size

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        var textName: TextView
        var textDescription: TextView
        var textEmail: TextView

        init {
            textName = itemView.findViewById(R.id.walker_name)
            textDescription = itemView.findViewById(R.id.walker_description)
            textEmail = itemView.findViewById(R.id.walker_email)
        }
    }
}