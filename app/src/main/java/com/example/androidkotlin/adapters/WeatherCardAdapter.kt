package com.example.androidkotlin.adapters

import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.appcompat.content.res.AppCompatResources
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.example.androidkotlin.R
import com.example.androidkotlin.services.WeatherService

class WeatherCardAdapter(private val hourlyWeather: List<WeatherService.HourlyWeather>): RecyclerView.Adapter<WeatherCardAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context).inflate(R.layout.weather_card_layout, parent, false)
        return ViewHolder(inflater)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.textCondition.text = hourlyWeather[position].condition
        holder.textTemp.text = hourlyWeather[position].temp
        holder.textTime.text = hourlyWeather[position].time
        var icon : Drawable? = when (hourlyWeather[position].icon) {
            "113" -> AppCompatResources.getDrawable(holder.itemView.context, R.drawable.wi_113)
            "116" -> AppCompatResources.getDrawable(holder.itemView.context, R.drawable.wi_116)
            "119" -> AppCompatResources.getDrawable(holder.itemView.context, R.drawable.wi_119)
            else -> null
        }
        holder.textCondition.setCompoundDrawablesWithIntrinsicBounds(icon, null, null, null)
    }

    override fun getItemCount(): Int {
        return hourlyWeather.size
    }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        var textCondition: TextView
        var textTemp: TextView
        var textTime: TextView

        init {
            textCondition = itemView.findViewById(R.id.text_hourly_condition)
            textTemp = itemView.findViewById(R.id.text_hourly_temp)
            textTime = itemView.findViewById(R.id.text_hourly_time)
        }
    }
}