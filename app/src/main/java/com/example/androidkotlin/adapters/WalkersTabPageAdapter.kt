package com.example.androidkotlin.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.androidkotlin.view.WalkersListFragment
import com.example.androidkotlin.view.WalkersNearFragment
import com.example.androidkotlin.view.WalkersSearchFragment
import com.example.androidkotlin.view.WalkersTopLikesFragment

class WalkersTabPageAdapter(activity: FragmentActivity, private val tabCount: Int): FragmentStateAdapter(activity) {
    override fun getItemCount(): Int = tabCount

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> WalkersSearchFragment()
            1 -> WalkersTopLikesFragment()
            2 -> WalkersNearFragment()
            else -> WalkersSearchFragment()
        }
    }

}