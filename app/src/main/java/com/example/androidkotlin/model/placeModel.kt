package com.example.androidkotlin.model

import com.google.firebase.firestore.DocumentId

data class PlaceModel (
    @DocumentId
    val documentId: String = "",
    val name: String = "",
    val description: String = "",
    val address: String = "",
    val type: String = "",
    val latitude: String = "0.0",
    val longitude: String = "0.0",
    val score: String = "",
    var distance: String = "",
)