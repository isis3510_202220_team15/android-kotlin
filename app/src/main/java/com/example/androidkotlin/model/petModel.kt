package com.example.androidkotlin.model

import com.example.androidkotlin.model.EventModel
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class PetModel (val userId: String? = "", //Document ID is actually the user id
                     val Image: String? = "",
                     val age: String? = "",
                     val breed: String? = "",
                     var name: String? = "",
                     val weight: String? = "",
                     val Events: MutableList<EventModel> = ArrayList(),
                     val PetsLiked: MutableList<String> = ArrayList())

data class OCRResponse (@SerializedName("texto")
                        @Expose
                        var texto: String)

data class OCRInput (
    @SerializedName("image")
    @Expose
    var image: String)