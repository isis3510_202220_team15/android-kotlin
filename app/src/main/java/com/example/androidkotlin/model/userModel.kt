package com.example.androidkotlin.model

data class UserModel (val userId: String? = "", //Document ID is actually the user id
                      val name: String? = "",
                      val birthDate: String? = "",
                      val phoneNumber: String? = "",
                      var email: String? = "")