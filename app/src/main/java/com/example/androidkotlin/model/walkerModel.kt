package com.example.androidkotlin.model

import com.google.firebase.firestore.DocumentId

data class WalkerModel (
    @DocumentId
    val documentId: String = "",
    val name: String? = "",
    var likes: ArrayList<String> =  ArrayList(),
    val description: String? = "",
    val city: String? = "",
    val neighborhood: String? = "",
    val address: String? = "",
    val latitude: Float? = 0f,
    val longitude: Float? = 0f,
    val radius: Int? = 0
)