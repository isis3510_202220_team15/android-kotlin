package com.example.androidkotlin.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class EventModel (val eventName: String, val eventDate: String, val eventDescription: String)