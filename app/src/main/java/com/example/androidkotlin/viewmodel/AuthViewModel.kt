package com.example.androidkotlin.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.auth.FirebaseAuth
import io.sentry.Sentry

class AuthViewModel : ViewModel() {

    private val _authState by lazy { MutableLiveData<AuthState>(AuthState.Idle) }
    val authState: LiveData<AuthState> = _authState

    fun signIn(email: String, password: String) {

        val transaction = Sentry.startTransaction("signInWithEmailAndPassword", "signIn")
        FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    _authState.value = AuthState.SuccessLogin
                } else {
                    task.exception?.let { e ->
                        Sentry.captureException(e)
                    }
                }
            }.addOnFailureListener { exception ->
                Sentry.captureException(exception)
                _authState.value = AuthState.AuthError(exception.localizedMessage)
            }
        transaction.finish()

    }

    fun signUp(email: String, password: String) {

        val transaction = Sentry.startTransaction("createUserWithEmailAndPassword", "signUp")
        FirebaseAuth.getInstance().createUserWithEmailAndPassword(email,password).addOnCompleteListener { task ->
            if(task.isSuccessful){
                _authState.value = AuthState.SuccessSignUp
            }
        }.addOnFailureListener { exception ->
            Sentry.captureException(exception)
            _authState.value = AuthState.AuthError(exception.localizedMessage)
        }
        transaction.finish()
    }

    sealed class AuthState {
        object Idle : AuthState()
        object SuccessLogin : AuthState()
        object SuccessSignUp : AuthState()
        class AuthError(val exceptionMessage: String? = null) : AuthState()
    }

}