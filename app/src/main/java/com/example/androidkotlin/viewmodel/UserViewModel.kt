package com.example.androidkotlin.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.androidkotlin.db.DBHelper
import com.example.androidkotlin.model.UserModel
import com.example.androidkotlin.services.CustomException
import com.example.androidkotlin.services.FirebaseService
import io.sentry.Sentry
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class UserViewModel : ViewModel() {
    private val _userModel = MutableLiveData<UserModel>()
    val userProfile: LiveData<UserModel> = _userModel
    private val _error: MutableLiveData<String>? = null
    val error: MutableLiveData<String>? = _error

    fun updateUserProfile(name : String, birthDate: String, phoneNumber: String){
        FirebaseService.updateUserProfile(name, birthDate, phoneNumber)
    }

    fun fetchUserProfile(){
        CoroutineScope(Dispatchers.IO).launch {
            _userModel.postValue(FirebaseService.getCurrentUserData())
        }
    }
}