package com.example.androidkotlin.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.androidkotlin.model.OCRInput
import com.example.androidkotlin.model.OCRResponse
import com.example.androidkotlin.model.PetModel
import com.example.androidkotlin.model.EventModel
import com.example.androidkotlin.services.APIService
import com.example.androidkotlin.services.FirebaseService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.*
import retrofit2.converter.gson.GsonConverterFactory
import java.util.ArrayList

class PetViewModel : ViewModel() {
    private val _petModel = MutableLiveData<PetModel>()
    private val _petsData = MutableLiveData<MutableList<PetModel>>()
    private val _petsLiked = MutableLiveData<MutableList<String>>()
    val petProfile: LiveData<PetModel> = _petModel
    val petsData: LiveData<MutableList<PetModel>> = _petsData
    val petsLiked: LiveData<MutableList<String>> = _petsLiked
    var createNewPetLiveData: MutableLiveData<OCRResponse?> = MutableLiveData()

    companion object {
        fun getOcr(): Retrofit {
            return Retrofit.Builder()
                .baseUrl("https://identificardoc-czt2z7swra-uk.a.run.app")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }
    }

    fun updatePetProfile(
        Image: String,
        age: String,
        breed: String,
        name: String,
        weight: String,
        events: MutableList<EventModel>,
        petsLiked: MutableList<String>
    ) {
        FirebaseService.updatePetProfile(Image, age, breed, name, weight, events, petsLiked)
    }

    fun updatePetImage(Image: String) {
        FirebaseService.updatePetImage(Image)
    }

    fun getOcrPetLiveDataObserver(): MutableLiveData<OCRResponse?> {
        return createNewPetLiveData
    }

    fun ocrPetImage(model: OCRInput) {
        CoroutineScope(Dispatchers.IO).launch {
            val retroService = getOcr().create(APIService::class.java)
            val call = retroService.getOCRFromImage(model)
            call.enqueue(object : Callback<OCRResponse> {
                override fun onResponse(call: Call<OCRResponse>, response: Response<OCRResponse>) {
                    if (response.isSuccessful) {
                        createNewPetLiveData.postValue(response.body())
                    } else {
                        createNewPetLiveData.postValue(null)
                    }
                }

                override fun onFailure(call: Call<OCRResponse>, t: Throwable) {
                    createNewPetLiveData.postValue(null)
                }
            })
        }
    }

    fun obtenerNuevoPet() {
        CoroutineScope(Dispatchers.IO).launch { _petModel.postValue(FirebaseService.getCurrentPetData()) }
    }

    fun createEventProfile(events: MutableList<EventModel>) {
        FirebaseService.updateEvent(events)
    }

    fun createPetsLiked(pets: MutableList<String>) {
        FirebaseService.updatePetsLiked(pets)
    }

    fun getPets() {
        CoroutineScope(Dispatchers.IO).launch { _petsData.postValue(FirebaseService.getCurrentPets()) }
    }

    fun getPetsLiked() {
        CoroutineScope(Dispatchers.IO).launch { _petsLiked.postValue(FirebaseService.getPetsLiked()) }
    }

    fun fetchPetProfile() {
        CoroutineScope(Dispatchers.IO).launch {
            _petModel.postValue(FirebaseService.getCurrentPetData())
        }
    }

    init {
        CoroutineScope(Dispatchers.IO).launch { _petModel.postValue(FirebaseService.getCurrentPetData()) }
    }
}