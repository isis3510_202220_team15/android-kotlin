package com.example.androidkotlin.viewmodel

import android.content.Context
import android.util.LruCache
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.androidkotlin.model.WalkerModel
import com.example.androidkotlin.network.ConnectionLiveData
import com.example.androidkotlin.repositories.WalkerRepository
import com.example.androidkotlin.services.FirebaseService
import kotlinx.coroutines.launch

class WalkersViewModel() : ViewModel() {
    private val _walkersModel = MutableLiveData<List<WalkerModel>>()
    val walkers: LiveData<List<WalkerModel>> = _walkersModel

    private val _walkerProfileModel = MutableLiveData<WalkerModel>()
    val walkerProfile: LiveData<WalkerModel> = _walkerProfileModel

    private val walkerRepository : WalkerRepository = WalkerRepository()

    init {
        getWalkerProfile()
    }

    fun updateCurrentWalkersData() {
        viewModelScope.launch { _walkersModel.value = walkerRepository.updateCurrentWalkersData() }
    }

    fun updateCurrentWalkersDataBySearch(field: String, value: String) {
        viewModelScope.launch { _walkersModel.value = walkerRepository.updateCurrentWalkersDataBySearch(field, value) }
    }

    fun updateCurrentWalkersDataSorted(sortedBy: String, ascending: Boolean, limit: Int) {
        viewModelScope.launch {_walkersModel.value = walkerRepository.updateCurrentWalkersDataSorted(sortedBy, ascending, limit)}
    }

    fun updateCurrentWalkersDataNear(lat: Double, lon: Double, context: Context) {
        viewModelScope.launch { walkerRepository.updateCurrentWalkersDataNear(lat, lon, context) { walkers ->
            _walkersModel.value = walkers
        }}
    }

    fun handleLikeButton(documentId: String){
        viewModelScope.launch { walkerRepository.handleLikeButton(documentId) }
    }

    fun handleDislikeButton(documentId: String){
        viewModelScope.launch { walkerRepository.handleDislikeButton(documentId) }
    }

    fun updateWalkerProfile(email: String, name: String, city: String, neighborhood: String, description: String, address: String, lat: Float, lon: Float, radius: Int) {
        viewModelScope.launch { walkerRepository.updateWalkerProfile(email, name, city, neighborhood, description, address, lat, lon, radius) }
    }

    fun getWalkerProfile() {
        viewModelScope.launch { _walkerProfileModel.value = walkerRepository.getWalkerProfile() }
    }

    fun saveWalkerProfileInCache(email: String?, name: String?, city: String?, neighborhood: String?, description: String?, address: String?, lat: Float?, lon: Float?, radius: Int?) {
        walkerRepository.saveWalkerProfileInCache(email, name, city, neighborhood, description, address, lat, lon, radius)
    }

    fun getWalkerProfileInCache(): WalkerModel {
        return walkerRepository.getWalkerProfileFromCache()
    }
}