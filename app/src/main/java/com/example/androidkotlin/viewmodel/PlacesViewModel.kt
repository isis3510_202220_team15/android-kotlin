package com.example.androidkotlin.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.android.volley.RequestQueue
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.example.androidkotlin.db.DBHelper
import com.example.androidkotlin.model.PlaceModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.json.JSONArray
import org.json.JSONObject

class PlacesViewModel(application: Application) : AndroidViewModel(application) {
    private val _placesModel = MutableLiveData<List<PlaceModel>>()
    private val _sortedPlacesModel = MutableLiveData<List<PlaceModel>>()
    private val _newPLacesModel = MutableLiveData<JSONArray>()

    private var currentLongitude: Double = 0.0
    private var currentLatitude: Double = 0.0
    val places: LiveData<List<PlaceModel>> = _placesModel
    val sortedPlaces: LiveData<List<PlaceModel>> = _sortedPlacesModel

    private val context = getApplication<Application>().applicationContext

    fun updatePlacesData(latitude: Double, longitude: Double) {
        CoroutineScope(Dispatchers.IO).launch {
            getPlaces(latitude, longitude) { places ->
                val db = DBHelper(context, null)
                if(places.isNotEmpty()){
                    db.resetDB()
                    for(place in places){
                        db.addPlace(place.documentId, place.name, place.description, place.address, place.type, place.latitude, place.longitude, place.score, place.distance)
                    }
                    _placesModel.postValue(places)
                    _sortedPlacesModel.postValue(places.sortedWith(compareBy { it.distance.toDouble() }))
                }
            }
        }
    }

    fun setLatitude(latitude: Double){
        currentLatitude = latitude
    }

    fun setLongitude(longitude: Double){
        currentLongitude = longitude
    }

    fun setPlacesData(newPlaces: List<PlaceModel>) {
            _placesModel.postValue(newPlaces)
    }

    fun setSortedData(newPlaces: List<PlaceModel>) {
        _sortedPlacesModel.postValue(newPlaces.sortedWith(compareBy { it.distance.toDouble() }))
    }

    private fun getPlaces(lat: Double, lng: Double, callbackSuccess: (List<PlaceModel>) -> Unit){
        val queue: RequestQueue = Volley.newRequestQueue(context)
        val url = "https://us-central1-moviles2022-1973a.cloudfunctions.net/getCPlaces"

        val jsonObject = JSONObject()
        jsonObject.put("lat", lat)
        jsonObject.put("lng", lng)
        val request = object: JsonObjectRequest(Method.POST, url, jsonObject, { response ->
            val places: JSONArray = response.getJSONArray("places")

                val placesObjects = mutableListOf<PlaceModel>()
                for (i in 0 until places.length()) {
                    val place = places.getJSONObject(i)
                    placesObjects.add(
                            PlaceModel(
                                name = place.getString("name"),
                                description = place.getString("description"),
                                address = place.getString("address"),
                                type = place.getString("type"),
                                latitude = place.getString("latitude"),
                                longitude = place.getString("longitude"),
                                score = place.getString("score"),
                                distance = place.getString("distance"),
                                documentId = place.getString("documentId")
                            )
                        )
                    }

            callbackSuccess(placesObjects.toList())
        }, { error ->
            Log.e("TAG", "ERROR RESPONSE IS $error")
        }){}
        queue.add(request)
    }
}